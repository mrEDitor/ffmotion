#ifndef _FFPLAYMOTION
#define _FFPLAYMOTION

#include <inttypes.h>
#include <math.h>
#include <limits.h>
#include <signal.h>
#include <stdint.h>
#include <assert.h>

#include <libavutil/avstring.h>
#include <libavutil/eval.h>
#include <libavutil/mathematics.h>
#include <libavutil/pixdesc.h>
#include <libavutil/imgutils.h>
#include <libavutil/dict.h>
#include <libavutil/bprint.h>
#include <libavutil/parseutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/avassert.h>
#include <libavutil/display.h>
#include <libavutil/time.h>
#include <libavformat/avformat.h>
#include <libavdevice/avdevice.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
#include <libavcodec/avfft.h>
#include <libswresample/swresample.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_thread.h>

#ifdef _WIN32
#include <windows.h>
#endif

/**
 *
 */
#define MAX_QUEUE_SIZE (15 * 1024 * 1024)

/**
 *
 */
#define MIN_FRAMES 25

/**
 *
 */
#define EXTERNAL_CLOCK_MIN_FRAMES 2

/**
 *
 */
#define EXTERNAL_CLOCK_MAX_FRAMES 10

/**
 * Minimum SDL audio buffer size, in samples.
 */
#define SDL_AUDIO_MIN_BUFFER_SIZE 512

/**
 * Calculate actual buffer size keeping in mind not cause too frequent audio callbacks.
 */
#define SDL_AUDIO_MAX_CALLBACKS_PER_SEC 30

/**
 * Step size for volume control in dB.
 */
#define SDL_VOLUME_STEP (0.75)

/**
 * No AV sync correction is done if below the minimum AV sync threshold.
 */
#define AV_SYNC_THRESHOLD_MIN 0.04

/**
 * AV sync correction is done if above the maximum AV sync threshold.
 */
#define AV_SYNC_THRESHOLD_MAX 0.1

/**
 * If a frame duration is longer than this, it will not be duplicated to compensate AV sync.
 */
#define AV_SYNC_FRAMEDUP_THRESHOLD 0.1

/**
 * No AV correction is done if too big error.
 */
#define AV_NOSYNC_THRESHOLD 10.0

/**
 * Maximum audio speed change to get correct sync.
 */
#define SAMPLE_CORRECTION_PERCENT_MAX 10

/**
 * External clock speed adjustment constants for realtime sources based on buffer fullness.
 */
#define EXTERNAL_CLOCK_SPEED_MIN  0.900

/**
 *
 */
#define EXTERNAL_CLOCK_SPEED_MAX  1.010

/**
 *
 */
#define EXTERNAL_CLOCK_SPEED_STEP 0.001

/**
 * We use about AUDIO_DIFF_AVG_NB A-V differences to make the average.
 */
#define AUDIO_DIFF_AVG_NB 20

/**
 * Polls for possible required screen refresh at least this often, should be less than 1/fps.
 */
#define REFRESH_RATE 0.01

//TODO: We assume that a decoded and resampled frame fits into this buffer
/**
 * NOTE: the size must be big enough to compensate the hardware audio buffer size size.
 */
#define SAMPLE_ARRAY_SIZE (8 * 65536)

/**
 *
 */
#define CURSOR_HIDE_DELAY 1000000

/**
 *
 */
#define USE_ONEPASS_SUBTITLE_RENDER 1

/**
 *
 */
#define VIDEO_PICTURE_QUEUE_SIZE 3

/**
 *
 */
#define SUBPICTURE_QUEUE_SIZE 16

/**
 *
 */
#define SAMPLE_QUEUE_SIZE 9

/**
 *
 */
#define FRAME_QUEUE_SIZE FFMAX(SAMPLE_QUEUE_SIZE, FFMAX(VIDEO_PICTURE_QUEUE_SIZE, SUBPICTURE_QUEUE_SIZE))

/**
 *
 */
#define FF_QUIT_EVENT    (SDL_USEREVENT + 2)

/**
 * Add indentation when printing to console.
 */
#define INDENT 1

/**
 * Show version when printing libs information.
 */
#define SHOW_VERSION 2

/**
 * Show configuration when printing libs information.
 */
#define SHOW_CONFIG 4

/**
 * Show copyright when printing program information.
 */
#define SHOW_COPYRIGHT 8

/**
 * Current year to be used when printing program information.
 */
#define CONFIG_THIS_YEAR 2019

/**
 * program version to be used when printing program information.
 */
#define FFPLAYMOTION_VERSION "0.1"

/**
 * gcc version to be used when printing program information.
 */
#ifdef __GNUC__
#define CC_IDENT "gcc " __VERSION__
#else
#define CC_IDENT "<unknown>"
#endif

/**
 * FFMPEG configuration to be used when printing program information.
 */
#define FFMPEG_CONFIGURATION "--disable-everything --disable-avdevice --disable-avfilter --disable-bzlib --disable-doc --disable-ffprobe --disable-lzo --disable-network --disable-postproc --disable-zlib --enable-fft --enable-rdft --enable-shared --disable-programs --disable-hwaccels --enable-hwaccel=h264_vda --disable-dxva2 --disable-vaapi --disable-vda --disable-vdpau --optflags=-O2 --enable-pic --enable-protocol=file --enable-encoder=libx264 --enable-encoder=libfaac --enable-muxer=mp4 --enable-muxer=rtp --extra-cflags='-I/home/admin/avaconverter/third_party/ffmpeg/../faac/files/faac-1.28/include -I/home/admin/avaconverter/third_party/ffmpeg/../x264/config/AvaConverter/linux/x64 -I/home/admin/avaconverter/third_party/ffmpeg/../x264/files/x264' --enable-libx264 --enable-gpl --enable-libfaac --enable-nonfree"

/**
 *
 */
#define CONFIG_AVUTIL 1

/**
 *
 */
#define CONFIG_AVCODEC 1

/**
 *
 */
#define CONFIG_AVFORMAT 1

/**
 * The libavdevice library provides a generic framework for grabbing from and
 * rendering to many common multimedia input/output devices, and supports several
 * input and output devices, including Video4Linux2, VfW, DShow, and ALSA.
 */
#define CONFIG_AVDEVICE 1

/**
 *
 */
#define CONFIG_AVFILTER 1

/**
 *
 */
#define CONFIG_AVRESAMPLE 1

/**
 *
 */
#define CONFIG_POSTPROC 1

/**
 *
 */
#define CONFIG_SWRESAMPLE 1

/**
 *
 */
#define CONFIG_SWSCALE 1

/**
 *
 */
#define LIBAVRESAMPLE_VERSION_MAJOR  3

/**
 *
 */
#define LIBAVRESAMPLE_VERSION_MINOR  0

/**
 *
 */
#define LIBAVRESAMPLE_VERSION_MICRO  0

/**
 *
 */
#define LIBAVRESAMPLE_VERSION_INT  AV_VERSION_INT(LIBAVRESAMPLE_VERSION_MAJOR, \
                                                  LIBAVRESAMPLE_VERSION_MINOR, \
                                                  LIBAVRESAMPLE_VERSION_MICRO)

/**
 *
 */
#define LIBAVRESAMPLE_VERSION          AV_VERSION(LIBAVRESAMPLE_VERSION_MAJOR, \
                                                  LIBAVRESAMPLE_VERSION_MINOR, \
                                                  LIBAVRESAMPLE_VERSION_MICRO)

/**
 *
 */
#define LIBAVRESAMPLE_BUILD        LIBAVRESAMPLE_VERSION_INT

/**
 *
 */
#define LIBAVRESAMPLE_IDENT "Lavr" AV_STRINGIFY(LIBAVRESAMPLE_VERSION)

/**
 *
 */
#define LIBPOSTPROC_VERSION_MAJOR  55

/**
 *
 */
#define LIBPOSTPROC_VERSION_MINOR   4

/**
 *
 */
#define LIBPOSTPROC_VERSION_MICRO 100

/**
 *
 */
#define LIBPOSTPROC_VERSION_INT AV_VERSION_INT(LIBPOSTPROC_VERSION_MAJOR, \
                                               LIBPOSTPROC_VERSION_MINOR, \
                                               LIBPOSTPROC_VERSION_MICRO)

/**
 *
 */
#define LIBPOSTPROC_VERSION     AV_VERSION(LIBPOSTPROC_VERSION_MAJOR, \
                                           LIBPOSTPROC_VERSION_MINOR, \
                                           LIBPOSTPROC_VERSION_MICRO)

/**
 *
 */
#define LIBPOSTPROC_BUILD       LIBPOSTPROC_VERSION_INT

/**
 *
 */
#define LIBPOSTPROC_IDENT "postproc" AV_STRINGIFY(LIBPOSTPROC_VERSION)

/**
 * Program Name.
 */
#define program_name "ffplaymotion"

/**
 * Program Birth Year.
 */
#define ffplay_birth_year 2003
#define player_birth_year 2018
#define motion_birth_year 2019

/**
 *
 */
extern unsigned sws_flags;

/**
 *
 */
typedef struct MyAVPacketList {
    AVPacket pkt;
    struct MyAVPacketList *next;
    int serial;
} MyAVPacketList;

/**
 *
 */
typedef struct PacketQueue {
    MyAVPacketList *first_pkt, *last_pkt;
    int nb_packets;
    int size;
    int64_t duration;
    int abort_request;
    int serial;
    SDL_mutex *mutex;
    SDL_cond *cond;
} PacketQueue;

/**
 *
 */
typedef struct AudioParams {
    int freq;
    int channels;
    int64_t channel_layout;
    enum AVSampleFormat fmt;
    int frame_size;
    int bytes_per_sec;
} AudioParams;

/**
 *
 */
typedef struct Clock {
    double pts;           /* clock base */
    double pts_drift;     /* clock base minus time at which we updated the clock */
    double last_updated;
    double speed;
    int serial;           /* clock is based on a packet with this serial */
    int paused;
    int *queue_serial;    /* pointer to the current packet queue serial, used for obsolete clock detection */
} Clock;

/**
 * Common struct for handling all types of decoded data and allocated render buffers.
 */
typedef struct Frame {
    AVFrame *frame;
    AVSubtitle sub;
    int serial;
    double pts;           /* presentation timestamp for the frame */
    double duration;      /* estimated duration of the frame */
    int64_t pos;          /* byte position of the frame in the input file */
    int width;
    int height;
    int format;
    AVRational sar;
    int uploaded;
    int flip_v;
} Frame;

/**
 *
 */
typedef struct FrameQueue {
    Frame queue[FRAME_QUEUE_SIZE];
    int rindex;
    int windex;
    int size;
    int max_size;
    int keep_last;
    int rindex_shown;
    SDL_mutex *mutex;
    SDL_cond *cond;
    PacketQueue *pktq;
} FrameQueue;

/**
 *
 */
enum {
    AV_SYNC_AUDIO_MASTER, /* default choice */
    AV_SYNC_VIDEO_MASTER,
    AV_SYNC_EXTERNAL_CLOCK, /* synchronize to an external clock */
};

/**
 *
 */
typedef struct Decoder {
    AVPacket pkt;
    PacketQueue *queue;
    AVCodecContext *avctx;
    int pkt_serial;
    int finished;
    int packet_pending;
    SDL_cond *empty_queue_cond;
    int64_t start_pts;
    AVRational start_pts_tb;
    int64_t next_pts;
    AVRational next_pts_tb;
    SDL_Thread *decoder_tid;
} Decoder;

/**
 *
 */
typedef struct VideoState {
    SDL_Thread *read_tid;
    AVInputFormat *iformat;
    int abort_request;
    int force_refresh;
    int paused;
    int last_paused;
    int queue_attachments_req;
    int seek_req;
    int seek_flags;
    int64_t seek_pos;
    int64_t seek_rel;
    int read_pause_return;
    AVFormatContext *ic;
    int realtime;

    Clock audclk;
    Clock vidclk;
    Clock extclk;

    FrameQueue pictq;
    FrameQueue subpq;
    FrameQueue sampq;

    Decoder auddec;
    Decoder viddec;
    Decoder subdec;

    int audio_stream;

    int av_sync_type;

    double audio_clock;
    int audio_clock_serial;
    double audio_diff_cum; /* used for AV difference average computation */
    double audio_diff_avg_coef;
    double audio_diff_threshold;
    int audio_diff_avg_count;
    AVStream *audio_st;
    PacketQueue audioq;
    int audio_hw_buf_size;
    uint8_t *audio_buf;
    uint8_t *audio_buf1;
    unsigned int audio_buf_size; /* in bytes */
    unsigned int audio_buf1_size;
    int audio_buf_index; /* in bytes */
    int audio_write_buf_size;
    int audio_volume;
    int muted;
    struct AudioParams audio_src;
    struct AudioParams audio_filter_src;
    struct AudioParams audio_tgt;
    struct SwrContext *swr_ctx;
    int frame_drops_early;
    int frame_drops_late;

    enum ShowMode {
        SHOW_MODE_NONE = -1,
        SHOW_MODE_VIDEO = 0,
        SHOW_MODE_WAVES,
        SHOW_MODE_RDFT,
        SHOW_MODE_NB
    } show_mode;

    int16_t sample_array[SAMPLE_ARRAY_SIZE];
    int sample_array_index;
    int last_i_start;
    RDFTContext *rdft;
    int rdft_bits;
    FFTSample *rdft_data;
    int xpos;
    double last_vis_time;
    SDL_Texture *vis_texture;
    SDL_Texture *sub_texture;
    SDL_Texture *vid_texture;
    SDL_Texture *motion_texture;

    int subtitle_stream;
    AVStream *subtitle_st;
    PacketQueue subtitleq;

    double frame_timer;
    double frame_last_returned_time;
    double frame_last_filter_delay;
    int video_stream;
    AVStream *video_st;
    PacketQueue videoq;
    double max_frame_duration;      // maximum duration of a frame - above this, we consider the jump a timestamp discontinuity
    struct SwsContext *img_convert_ctx;
    struct SwsContext *sub_convert_ctx;
    int eof;

    char *filename;
    int width;
    int height;
    int xleft;
    int ytop;
    int step;

    int vfilter_idx;
    AVFilterContext *in_video_filter;   // the first filter in the video chain
    AVFilterContext *out_video_filter;  // the last filter in the video chain
    AVFilterContext *in_audio_filter;   // the first filter in the audio chain
    AVFilterContext *out_audio_filter;  // the last filter in the audio chain
    AVFilterGraph *agraph;              // audio filter graph

    int last_video_stream;
    int last_audio_stream;
    int last_subtitle_stream;

    SDL_cond *continue_read_thread;
} VideoState;

/**
 * Options specified by the user.
 */

//
AVInputFormat *file_iformat;

//
const char *input_filename;

//
const char *window_title;

//
static int default_width = 640;

//
static int default_height = 480;

//
extern int screen_width;

//
extern int screen_height;

//
extern int screen_left;

//
extern int screen_top;

//
extern int show_motion;

//
extern int show_onlymotion;

// set this to disable audio output
extern int audio_disable;

//
extern int video_disable;

//
extern int subtitle_disable;

//
extern const char *wanted_stream_spec[AVMEDIA_TYPE_NB];

//
extern int seek_by_bytes;

//
extern float seek_interval;

// set this to disable output to display
extern int display_disable;

//
extern int borderless;

//
extern int startup_volume;

//
extern int show_status;

//
extern int av_sync_type;

//
extern int64_t start_time;

//
extern int64_t duration;

//
extern int fast;

//
extern int genpts;

//
extern int lowres;

//
extern int decoder_reorder_pts;

//
extern int autoexit;

//
extern int exit_on_keydown;

//
extern int exit_on_mousedown;

//
extern int loop;

//
extern int framedrop;

//
extern int infinite_buffer;

//
extern enum ShowMode show_mode;

//
const char *audio_codec_name;

//
extern const char *subtitle_codec_name;

//
extern const char *video_codec_name;

//
extern double rdftspeed;

//
extern int64_t cursor_last_shown;

//
extern int cursor_hidden;

//
extern const char **vfilters_list;

//
extern int nb_vfilters;

//
extern char *afilters;

//
extern int autorotate;

//
extern int find_stream_info;

//
extern int is_full_screen;

//
extern int64_t audio_callback_time;

//
extern AVPacket flush_pkt;

//
extern SDL_Window *window;

//
extern SDL_Renderer *renderer;

//
extern SDL_RendererInfo renderer_info;

//
extern SDL_AudioDeviceID audio_dev;

/**
 *
 */
typedef struct {
    enum AVPixelFormat format;
    int texture_fmt;
} TextureFormatEntry;
extern const TextureFormatEntry sdl_texture_format_map[];

extern AVDictionary *sws_dict;
extern AVDictionary *swr_opts;
extern AVDictionary *format_opts, *codec_opts, *resample_opts;

/**
 * Command line option definition.
 */
typedef struct OptionDefinition {
    const char *name;
    int flags;
#define HAS_ARG    0x0001
#define OPT_BOOL   0x0002
#define OPT_EXPERT 0x0004
#define OPT_STRING 0x0008
#define OPT_VIDEO  0x0010
#define OPT_AUDIO  0x0020
#define OPT_INT    0x0080
#define OPT_FLOAT  0x0100
#define OPT_SUBTITLE 0x0200
#define OPT_INT64  0x0400
#define OPT_EXIT   0x0800
#define OPT_DATA   0x1000
#define OPT_PERFILE  0x2000     /* the option is per-file (currently ffmpeg-only).
                                       implied by OPT_OFFSET or OPT_SPEC */
#define OPT_OFFSET 0x4000       /* option is specified as an offset in a passed optctx */
#define OPT_SPEC   0x8000       /* option is to be stored in an array of SpecifierOpt.
                                       Implies OPT_OFFSET. Next element after the offset is
                                       an int containing element count in the array. */
#define OPT_TIME  0x10000
#define OPT_DOUBLE 0x20000
#define OPT_INPUT  0x40000
#define OPT_OUTPUT 0x80000

    union {
        void *dst_ptr;

        int (*func_arg)(void *, const char *, const char *);

        size_t off;
    } u;

    const char *help;
    const char *argname;
} OptionDefinition;

/**
 *
 */
enum show_muxdemuxers {
    SHOW_DEFAULT,
    SHOW_DEMUXERS,
    SHOW_MUXERS,
};

/**
 *
 */
extern FILE *report_file;

/**
 *
 */
extern int report_file_level;

/**
 *
 */
typedef struct SpecifierOpt {
    char *specifier;    /**< stream/chapter/program/... specifier */

    union {
        uint8_t *str;
        int i;
        int64_t i64;
        uint64_t ui64;
        float f;
        double dbl;
    } u;
} SpecifierOpt;

/**
 * Libraries configuration mismatch found flag.
 */
extern int warned_cfg;

/**
 * Uses av_log to print information (version or configuration based on the given flags)
 * for the given library.
 */
#define PRINT_LIB_INFO(libname, LIBNAME, flags, level)                  \
    if (CONFIG_##LIBNAME) {                                             \
        const char *indent = flags & INDENT? "  " : "";                 \
        if (flags & SHOW_VERSION) {                                     \
            unsigned int version = libname##_version();                 \
            av_log(NULL, level,                                         \
                   "%slib%-11s %2d.%3d.%3d / %2d.%3d.%3d\n",            \
                   indent, #libname,                                    \
                   LIB##LIBNAME##_VERSION_MAJOR,                        \
                   LIB##LIBNAME##_VERSION_MINOR,                        \
                   LIB##LIBNAME##_VERSION_MICRO,                        \
                   AV_VERSION_MAJOR(version), AV_VERSION_MINOR(version),\
                   AV_VERSION_MICRO(version));                          \
        }                                                               \
        if (flags & SHOW_CONFIG) {                                      \
            const char *cfg = libname##_configuration();                \
            if (strcmp(FFMPEG_CONFIGURATION, cfg)) {                    \
                if (!warned_cfg) {                                      \
                    av_log(NULL, level,                                 \
                            "%sWARNING: library configuration mismatch\n", \
                            indent);                                    \
                    warned_cfg = 1;                                     \
                }                                                       \
                av_log(NULL, level, "%s%-11s configuration: %s\n",      \
                        indent, #libname, cfg);                         \
            }                                                           \
        }                                                               \
    }                                                                   \


/**
 * Initializes dynamic library loading.
 */
void init_dynload(void);

/**
 * Finds the '-loglevel' option in the command line args and applies it.
 *
 * @param   argc    command line arguments counter.
 * @param   argv    command line arguments.
 * @param   options program command line options definition list
 */
void parse_loglevel(int argc, char **argv, const OptionDefinition *options);

/**
 * Locates the given option name in the given command line arguments and finds the
 * related option definition in the program options definition list.
 *
 * @param   argc        command line arguments counter.
 * @param   argv        command line arguments.
 * @param   options     program options definition list.
 * @param   option_name the name of the option to be found.
 *
 * @return              the options index found in the command line arguments, 0 otherwise.
 */
int locate_option(int argc, char **argv, const OptionDefinition *options, const char *option_name);

/**
 * Finds and returns the Option Definition for the given option name in the given program
 * options definition list.
 *
 * @param   options program options definition list.
 * @param   name    the name of the option to search for.
 *
 * @return          the Option Definition for the fiven option name.
 */
const OptionDefinition *find_option(const OptionDefinition *options, const char *name);

/**
 *
 * @param options
 */
void check_options(const OptionDefinition *options);

/**
 * Initialize the cmdutils option system, in particular
 * allocate the *_opts contexts.
 */
void init_opts(void);

/**
 * Uninitialize the cmdutils option system, in particular
 * free the *_opts contexts and their contents.
 */
void uninit_opts(void);

/**
 * Termination request signals handler.
 *
 * @param   sig
 */
void sigterm_handler(int sig);

/**
 * Print the program banner to stderr. The banner contents depend on the
 * current version of the repository and of the libav* libraries used by
 * the program.
 *
 * @param   argc    command line arguments counter.
 * @param   argv    command line arguments.
 * @param   options program command line options definition list
 */
void show_banner(int argc, char **argv, const OptionDefinition *options);

/**
 * Prints program information (FFMPEG version, copyright, gcc and configuration.
 *
 * @param   flags   INDENT:         indent output message
 *                  SHOW_COPYRIGHT: print copyright message
 * @param   level   the importance level of the message
 */
void print_program_info(int flags, int level);

/**
 * Prints all libraries information (verion or configuration based on flags).
 *
 * @param   flags   INDENT:         indent output message
 *                  SHOW_VERSION:   print library version
 *                  SHOW_CONFIG:    print library configuration
 * @param   level   the importance level of the message
 */
void print_all_libs_info(int flags, int level);

/**
 * Shows program usage information.
 */
void show_usage(void);

void (*program_exit)(int ret);

/**
 * Wraps exit with a program-specific cleanup routine.
 */
void exit_program(int ret) av_noreturn;

/**
 * Realloc array to hold new_size elements of elem_size.
 * Calls exit() on failure.
 *
 * @param array array to reallocate
 * @param elem_size size in bytes of each element
 * @param size new element count will be written here
 * @param new_size number of elements to place in reallocated array
 * @return reallocated array
 */
void *grow_array(void *array, int elem_size, int *size, int new_size);

const OptionDefinition program_options[];

/**
 * Suppress printing banner.
 *
 * All FFmpeg tools will normally show a copyright notice, build options and library
 * versions. This option can be used to suppress printing this information.
 */
int hide_banner;

VideoState *stream_open(const char *filename, AVInputFormat *iformat);

void stream_close(VideoState *is);

void stream_cycle_channel(VideoState *is, int codec_type);

double get_clock(Clock *c);

void video_refresh(void *opaque, double *remaining_time);

double get_master_clock(VideoState *is);

void stream_seek(VideoState *is, int64_t pos, int64_t rel, int seek_by_bytes);

void toggle_pause(VideoState *is);

void update_volume(VideoState *is, int sign, double step);

void step_to_next_frame(VideoState *is);

int64_t frame_queue_last_pos(FrameQueue *f);

const AVClass av_resample_context_class;

/**
 * Parse a string and return its corresponding value as a double.
 * Exit from the application if the string cannot be correctly
 * parsed or the corresponding value is invalid.
 *
 * @param context the context of the value to be set (e.g. the
 * corresponding command line option name)
 * @param numstr the string to be parsed
 * @param type the type (OPT_INT64 or OPT_FLOAT) as which the
 * string should be parsed
 * @param min the minimum valid accepted value
 * @param max the maximum valid accepted value
 */
double parse_number_or_die(const char *context, const char *numstr, int type,
                           double min, double max);

/**
 * Parse a string specifying a time and return its corresponding
 * value as a number of microseconds. Exit from the application if
 * the string cannot be correctly parsed.
 *
 * @param context the context of the value to be set (e.g. the
 * corresponding command line option name)
 * @param timestr the string to be parsed
 * @param is_duration a flag which tells how to interpret timestr, if
 * not zero timestr is interpreted as a duration, otherwise as a
 * date
 *
 * @see av_parse_time()
 */
int64_t parse_time_or_die(const char *context, const char *timestr,
                          int is_duration);

#endif