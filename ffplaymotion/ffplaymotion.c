/**
 *
 *   File:   player-sdl.c
 *           Full FFmpeg player forked from the original FFplay.
 *
 *   Author: Rambod Rahmani (https://github.com/rambodrahmani/ffmpeg-video-player)
 *           Created on 11/27/18.
 *
 *   Modified
 *
 **/
#include "ffplaymotion.h"

unsigned sws_flags = SWS_BICUBIC;
int show_motion = 1;
int show_onlymotion = 0;
int screen_width  = 0;
int screen_height = 0;
int screen_left = SDL_WINDOWPOS_CENTERED;
int screen_top = SDL_WINDOWPOS_CENTERED;
int audio_disable;
int video_disable;
int subtitle_disable;
int seek_by_bytes = -1;
float seek_interval = 10;
int display_disable;
int borderless;
int startup_volume = 100;
int show_status = 1;
int av_sync_type = AV_SYNC_AUDIO_MASTER;
int64_t start_time = AV_NOPTS_VALUE;
int64_t duration = AV_NOPTS_VALUE;
int fast = 0;
int genpts = 0;
int lowres = 0;
int decoder_reorder_pts = -1;
int autoexit;
int exit_on_keydown;
int exit_on_mousedown;
int loop = 1;
int framedrop = -1;
int infinite_buffer = -1;
double rdftspeed;
int64_t cursor_last_shown;
int cursor_hidden = 0;
int nb_vfilters = 0;
char *afilters = NULL;
const char **vfilters_list = NULL;
const char *subtitle_codec_name;
const char *video_codec_name;
int autorotate = 1;
int find_stream_info = 1;
int is_full_screen;
int64_t audio_callback_time;
AVPacket flush_pkt;
SDL_Window *window;
SDL_Renderer *renderer;
SDL_RendererInfo renderer_info = {0};
SDL_AudioDeviceID audio_dev;
AVDictionary *sws_dict;
AVDictionary *swr_opts;
AVDictionary *format_opts, *codec_opts, *resample_opts;
const char* wanted_stream_spec[AVMEDIA_TYPE_NB] = {0};
enum ShowMode show_mode = SHOW_MODE_NONE;
FILE *report_file;
int report_file_level = AV_LOG_DEBUG;
int warned_cfg = 0;

/**
 * Register a program-specific cleanup routine.
 */
void register_exit(void (*cb)(int ret))
{
    program_exit = cb;
}

void do_exit(VideoState *is)
{
    if (is) {
        stream_close(is);
    }
    if (renderer)
        SDL_DestroyRenderer(renderer);
    if (window)
        SDL_DestroyWindow(window);
    uninit_opts();

    av_freep(&vfilters_list);

    avformat_network_deinit();
    if (show_status)
        printf("\n");
    SDL_Quit();
    av_log(NULL, AV_LOG_QUIET, "%s", "");

    // terminate program execution with 0
    exit(0);
}

void toggle_full_screen(VideoState *is)
{
    is_full_screen = !is_full_screen;
    SDL_SetWindowFullscreen(window, is_full_screen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
}

void toggle_audio_display(VideoState *is)
{
    int next = is->show_mode;
    do {
        next = (next + 1) % SHOW_MODE_NB;
    } while (next != is->show_mode && (next == SHOW_MODE_VIDEO && !is->video_st || next != SHOW_MODE_VIDEO && !is->audio_st));
    if (is->show_mode != next) {
        is->force_refresh = 1;
        is->show_mode = next;
    }
}

void refresh_loop_wait_event(VideoState *is, SDL_Event *event) {
    double remaining_time = 0.0;
    SDL_PumpEvents();
    while (!SDL_PeepEvents(event, 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT)) {
        if (!cursor_hidden && av_gettime_relative() - cursor_last_shown > CURSOR_HIDE_DELAY) {
            SDL_ShowCursor(0);
            cursor_hidden = 1;
        }
        if (remaining_time > 0.0)
            av_usleep((int64_t)(remaining_time * 1000000.0));
        remaining_time = REFRESH_RATE;
        if (is->show_mode != SHOW_MODE_NONE && (!is->paused || is->force_refresh))
            video_refresh(is, &remaining_time);
        SDL_PumpEvents();
    }
}

void seek_chapter(VideoState *is, int incr)
{
    int64_t pos = get_master_clock(is) * AV_TIME_BASE;
    int i;

    if (!is->ic->nb_chapters)
        return;

    /* find the current chapter */
    for (i = 0; i < is->ic->nb_chapters; i++) {
        AVChapter *ch = is->ic->chapters[i];
        if (av_compare_ts(pos, AV_TIME_BASE_Q, ch->start, ch->time_base) < 0) {
            i--;
            break;
        }
    }

    i += incr;
    i = FFMAX(i, 0);
    if (i >= is->ic->nb_chapters)
        return;

    av_log(NULL, AV_LOG_VERBOSE, "Seeking to chapter %d.\n", i);
    stream_seek(is, av_rescale_q(is->ic->chapters[i]->start, is->ic->chapters[i]->time_base,
                                 AV_TIME_BASE_Q), 0, 0);
}

void toggle_mute(VideoState *is)
{
    is->muted = !is->muted;
}

void toggle_motion(VideoState *is)
{
    show_motion = !show_motion;
}


/* handle an event sent by the GUI */
void event_loop(VideoState *cur_stream)
{
    SDL_Event event;
    double incr, pos, frac;

    for (;;) {
        double x;
        refresh_loop_wait_event(cur_stream, &event);
        switch (event.type) {
            case SDL_KEYDOWN:
                if (exit_on_keydown || event.key.keysym.sym == SDLK_ESCAPE || event.key.keysym.sym == SDLK_q) {
                    do_exit(cur_stream);
                    break;
                }
                // If we don't yet have a window, skip all key events, because read_thread might still be initializing...
                if (!cur_stream->width)
                    continue;
                switch (event.key.keysym.sym) {
                    case SDLK_f:
                        toggle_full_screen(cur_stream);
                        cur_stream->force_refresh = 1;
                        break;
                    case SDLK_p:
                    case SDLK_SPACE:
                        toggle_pause(cur_stream);
                        break;
                    case SDLK_m:
                        toggle_motion(cur_stream);
                        break;
                    case SDLK_n:
                        show_onlymotion = !show_onlymotion;
                        break;
                    case SDLK_KP_MULTIPLY:
                    case SDLK_0:
                        update_volume(cur_stream, 1, SDL_VOLUME_STEP);
                        break;
                    case SDLK_KP_DIVIDE:
                    case SDLK_9:
                        update_volume(cur_stream, -1, SDL_VOLUME_STEP);
                        break;
                    case SDLK_s: // S: Step to next frame
                        step_to_next_frame(cur_stream);
                        break;
                    case SDLK_a:
                        stream_cycle_channel(cur_stream, AVMEDIA_TYPE_AUDIO);
                        break;
                    case SDLK_v:
                        stream_cycle_channel(cur_stream, AVMEDIA_TYPE_VIDEO);
                        break;
                    case SDLK_c:
                        stream_cycle_channel(cur_stream, AVMEDIA_TYPE_VIDEO);
                        stream_cycle_channel(cur_stream, AVMEDIA_TYPE_AUDIO);
                        stream_cycle_channel(cur_stream, AVMEDIA_TYPE_SUBTITLE);
                        break;
                    case SDLK_t:
                        stream_cycle_channel(cur_stream, AVMEDIA_TYPE_SUBTITLE);
                        break;
                    case SDLK_w:
                        if (cur_stream->show_mode == SHOW_MODE_VIDEO && cur_stream->vfilter_idx < nb_vfilters - 1) {
                            if (++cur_stream->vfilter_idx >= nb_vfilters)
                                cur_stream->vfilter_idx = 0;
                        } else {
                            cur_stream->vfilter_idx = 0;
                            toggle_audio_display(cur_stream);
                        }
                        break;
                    case SDLK_PAGEUP:
                        if (cur_stream->ic->nb_chapters <= 1) {
                            incr = 600.0;
                            goto do_seek;
                        }
                        seek_chapter(cur_stream, 1);
                        break;
                    case SDLK_PAGEDOWN:
                        if (cur_stream->ic->nb_chapters <= 1) {
                            incr = -600.0;
                            goto do_seek;
                        }
                        seek_chapter(cur_stream, -1);
                        break;
                    case SDLK_LEFT:
                        incr = seek_interval ? -seek_interval : -10.0;
                        goto do_seek;
                    case SDLK_RIGHT:
                        incr = seek_interval ? seek_interval : 10.0;
                        goto do_seek;
                    case SDLK_UP:
                        incr = 60.0;
                        goto do_seek;
                    case SDLK_DOWN:
                        incr = -60.0;
                    do_seek:
                        if (seek_by_bytes) {
                            pos = -1;
                            if (pos < 0 && cur_stream->video_stream >= 0)
                                pos = frame_queue_last_pos(&cur_stream->pictq);
                            if (pos < 0 && cur_stream->audio_stream >= 0)
                                pos = frame_queue_last_pos(&cur_stream->sampq);
                            if (pos < 0)
                                pos = avio_tell(cur_stream->ic->pb);
                            if (cur_stream->ic->bit_rate)
                                incr *= cur_stream->ic->bit_rate / 8.0;
                            else
                                incr *= 180000.0;
                            pos += incr;
                            stream_seek(cur_stream, pos, incr, 1);
                        } else {
                            pos = get_master_clock(cur_stream);
                            if (isnan(pos))
                                pos = (double)cur_stream->seek_pos / AV_TIME_BASE;
                            pos += incr;
                            if (cur_stream->ic->start_time != AV_NOPTS_VALUE && pos < cur_stream->ic->start_time / (double)AV_TIME_BASE)
                                pos = cur_stream->ic->start_time / (double)AV_TIME_BASE;
                            stream_seek(cur_stream, (int64_t)(pos * AV_TIME_BASE), (int64_t)(incr * AV_TIME_BASE), 0);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (exit_on_mousedown) {
                    do_exit(cur_stream);
                    break;
                }
                if (event.button.button == SDL_BUTTON_LEFT) {
                    static int64_t last_mouse_left_click = 0;
                    if (av_gettime_relative() - last_mouse_left_click <= 500000) {
                        toggle_full_screen(cur_stream);
                        cur_stream->force_refresh = 1;
                        last_mouse_left_click = 0;
                    } else {
                        last_mouse_left_click = av_gettime_relative();
                    }
                }
            case SDL_MOUSEMOTION:
                if (cursor_hidden) {
                    SDL_ShowCursor(1);
                    cursor_hidden = 0;
                }
                cursor_last_shown = av_gettime_relative();
                if (event.type == SDL_MOUSEBUTTONDOWN) {
                    if (event.button.button != SDL_BUTTON_RIGHT)
                        break;
                    x = event.button.x;
                } else {
                    if (!(event.motion.state & SDL_BUTTON_RMASK))
                        break;
                    x = event.motion.x;
                }
                if (seek_by_bytes || cur_stream->ic->duration <= 0) {
                    uint64_t size =  avio_size(cur_stream->ic->pb);
                    stream_seek(cur_stream, size*x/cur_stream->width, 0, 1);
                } else {
                    int64_t ts;
                    int ns, hh, mm, ss;
                    int tns, thh, tmm, tss;
                    tns  = cur_stream->ic->duration / 1000000LL;
                    thh  = tns / 3600;
                    tmm  = (tns % 3600) / 60;
                    tss  = (tns % 60);
                    frac = x / cur_stream->width;
                    ns   = frac * tns;
                    hh   = ns / 3600;
                    mm   = (ns % 3600) / 60;
                    ss   = (ns % 60);
                    av_log(NULL, AV_LOG_INFO,
                           "Seek to %2.0f%% (%2d:%02d:%02d) of total duration (%2d:%02d:%02d)       \n", frac*100,
                           hh, mm, ss, thh, tmm, tss);
                    ts = frac * cur_stream->ic->duration;
                    if (cur_stream->ic->start_time != AV_NOPTS_VALUE)
                        ts += cur_stream->ic->start_time;
                    stream_seek(cur_stream, ts, 0, 0);
                }
                break;
            case SDL_WINDOWEVENT:
                switch (event.window.event) {
                    case SDL_WINDOWEVENT_RESIZED:
                        screen_width  = cur_stream->width  = event.window.data1;
                        screen_height = cur_stream->height = event.window.data2;
                        if (cur_stream->vis_texture) {
                            SDL_DestroyTexture(cur_stream->vis_texture);
                            cur_stream->vis_texture = NULL;
                        }
                    case SDL_WINDOWEVENT_EXPOSED:
                        cur_stream->force_refresh = 1;
                }
                break;
            case SDL_QUIT:
            case FF_QUIT_EVENT:
                do_exit(cur_stream);
                break;
            default:
                break;
        }
    }
}

typedef struct AudioData AudioData;
typedef struct AudioConvert AudioConvert;
typedef struct AudioMix AudioMix;
typedef struct ResampleContext ResampleContext;

enum RemapPoint {
    REMAP_NONE,
    REMAP_IN_COPY,
    REMAP_IN_CONVERT,
    REMAP_OUT_COPY,
    REMAP_OUT_CONVERT,
};

#define AVRESAMPLE_MAX_CHANNELS 32

typedef struct AVAudioResampleContext AVAudioResampleContext;

/**
 * @deprecated use libswresample
 *
 * Mixing Coefficient Types */
enum attribute_deprecated AVMixCoeffType {
    AV_MIX_COEFF_TYPE_Q8,   /** 16-bit 8.8 fixed-point                      */
    AV_MIX_COEFF_TYPE_Q15,  /** 32-bit 17.15 fixed-point                    */
    AV_MIX_COEFF_TYPE_FLT,  /** floating-point                              */
    AV_MIX_COEFF_TYPE_NB,   /** Number of coeff types. Not part of ABI      */
};

/**
 * @deprecated use libswresample
 *
 * Resampling Filter Types */
enum attribute_deprecated AVResampleFilterType {
    AV_RESAMPLE_FILTER_TYPE_CUBIC,              /**< Cubic */
    AV_RESAMPLE_FILTER_TYPE_BLACKMAN_NUTTALL,   /**< Blackman Nuttall Windowed Sinc */
    AV_RESAMPLE_FILTER_TYPE_KAISER,             /**< Kaiser Windowed Sinc */
};

/**
 * @deprecated use libswresample
 */
enum attribute_deprecated AVResampleDitherMethod {
    AV_RESAMPLE_DITHER_NONE,            /**< Do not use dithering */
    AV_RESAMPLE_DITHER_RECTANGULAR,     /**< Rectangular Dither */
    AV_RESAMPLE_DITHER_TRIANGULAR,      /**< Triangular Dither*/
    AV_RESAMPLE_DITHER_TRIANGULAR_HP,   /**< Triangular Dither with High Pass */
    AV_RESAMPLE_DITHER_TRIANGULAR_NS,   /**< Triangular Dither with Noise Shaping */
    AV_RESAMPLE_DITHER_NB,              /**< Number of dither types. Not part of ABI. */
};

typedef struct ChannelMapInfo {
    int channel_map[AVRESAMPLE_MAX_CHANNELS];   /**< source index of each output channel, -1 if not remapped */
    int do_remap;                               /**< remap needed */
    int channel_copy[AVRESAMPLE_MAX_CHANNELS];  /**< dest index to copy from */
    int do_copy;                                /**< copy needed */
    int channel_zero[AVRESAMPLE_MAX_CHANNELS];  /**< dest index to zero */
    int do_zero;                                /**< zeroing needed */
    int input_map[AVRESAMPLE_MAX_CHANNELS];     /**< dest index of each input channel */
} ChannelMapInfo;

typedef struct AVFifoBuffer {
    uint8_t *buffer;
    uint8_t *rptr, *wptr, *end;
    uint32_t rndx, wndx;
} AVFifoBuffer;

struct AVAudioFifo {
    AVFifoBuffer **buf;             /**< single buffer for interleaved, per-channel buffers for planar */
    int nb_buffers;                 /**< number of buffers */
    int nb_samples;                 /**< number of samples currently in the FIFO */
    int allocated_samples;          /**< current allocated size, in samples */

    int channels;                   /**< number of channels */
    enum AVSampleFormat sample_fmt; /**< sample format */
    int sample_size;                /**< size, in bytes, of one sample in a buffer */
};

/**
 * Context for an Audio FIFO Buffer.
 *
 * - Operates at the sample level rather than the byte level.
 * - Supports multiple channels with either planar or packed sample format.
 * - Automatic reallocation when writing to a full buffer.
 */
typedef struct AVAudioFifo AVAudioFifo;

struct AVAudioResampleContext {
    const AVClass *av_class;        /**< AVClass for logging and AVOptions  */

    uint64_t in_channel_layout;                 /**< input channel layout   */
    enum AVSampleFormat in_sample_fmt;          /**< input sample format    */
    int in_sample_rate;                         /**< input sample rate      */
    uint64_t out_channel_layout;                /**< output channel layout  */
    enum AVSampleFormat out_sample_fmt;         /**< output sample format   */
    int out_sample_rate;                        /**< output sample rate     */
    enum AVSampleFormat internal_sample_fmt;    /**< internal sample format */
    enum AVMixCoeffType mix_coeff_type;         /**< mixing coefficient type */
    double center_mix_level;                    /**< center mix level       */
    double surround_mix_level;                  /**< surround mix level     */
    double lfe_mix_level;                       /**< lfe mix level          */
    int normalize_mix_level;                    /**< enable mix level normalization */
    int force_resampling;                       /**< force resampling       */
    int filter_size;                            /**< length of each FIR filter in the resampling filterbank relative to the cutoff frequency */
    int phase_shift;                            /**< log2 of the number of entries in the resampling polyphase filterbank */
    int linear_interp;                          /**< if 1 then the resampling FIR filter will be linearly interpolated */
    double cutoff;                              /**< resampling cutoff frequency. 1.0 corresponds to half the output sample rate */
    enum AVResampleFilterType filter_type;      /**< resampling filter type */
    int kaiser_beta;                            /**< beta value for Kaiser window (only applicable if filter_type == AV_FILTER_TYPE_KAISER) */
    enum AVResampleDitherMethod dither_method;  /**< dither method          */

    int in_channels;        /**< number of input channels                   */
    int out_channels;       /**< number of output channels                  */
    int resample_channels;  /**< number of channels used for resampling     */
    int downmix_needed;     /**< downmixing is needed                       */
    int upmix_needed;       /**< upmixing is needed                         */
    int mixing_needed;      /**< either upmixing or downmixing is needed    */
    int resample_needed;    /**< resampling is needed                       */
    int in_convert_needed;  /**< input sample format conversion is needed   */
    int out_convert_needed; /**< output sample format conversion is needed  */
    int in_copy_needed;     /**< input data copy is needed                  */

    AudioData *in_buffer;           /**< buffer for converted input         */
    AudioData *resample_out_buffer; /**< buffer for output from resampler   */
    AudioData *out_buffer;          /**< buffer for converted output        */
    AVAudioFifo *out_fifo;          /**< FIFO for output samples            */

    AudioConvert *ac_in;        /**< input sample format conversion context  */
    AudioConvert *ac_out;       /**< output sample format conversion context */
    ResampleContext *resample;  /**< resampling context                      */
    AudioMix *am;               /**< channel mixing context                  */
    enum AVMatrixEncoding matrix_encoding;      /**< matrixed stereo encoding */

    /**
     * mix matrix
     * only used if avresample_set_matrix() is called before avresample_open()
     */
    double *mix_matrix;

    int use_channel_map;
    enum RemapPoint remap_point;
    ChannelMapInfo ch_map_info;
};

typedef struct AVAudioResampleContext AVAudioResampleContext;

#define OFFSET(x) offsetof(AVAudioResampleContext, x)
#define PARAM AV_OPT_FLAG_AUDIO_PARAM

const AVOption avresample_options[] = {
        { "in_channel_layout",      "Input Channel Layout",     OFFSET(in_channel_layout),      AV_OPT_TYPE_INT64,  { .i64 = 0              }, INT64_MIN,            INT64_MAX,              PARAM },
        { "in_sample_fmt",          "Input Sample Format",      OFFSET(in_sample_fmt),          AV_OPT_TYPE_INT,    { .i64 = AV_SAMPLE_FMT_S16 }, AV_SAMPLE_FMT_U8,     AV_SAMPLE_FMT_NB-1,     PARAM },
        { "in_sample_rate",         "Input Sample Rate",        OFFSET(in_sample_rate),         AV_OPT_TYPE_INT,    { .i64 = 48000          }, 1,                    INT_MAX,                PARAM },
        { "out_channel_layout",     "Output Channel Layout",    OFFSET(out_channel_layout),     AV_OPT_TYPE_INT64,  { .i64 = 0              }, INT64_MIN,            INT64_MAX,              PARAM },
        { "out_sample_fmt",         "Output Sample Format",     OFFSET(out_sample_fmt),         AV_OPT_TYPE_INT,    { .i64 = AV_SAMPLE_FMT_S16 }, AV_SAMPLE_FMT_U8,     AV_SAMPLE_FMT_NB-1,     PARAM },
        { "out_sample_rate",        "Output Sample Rate",       OFFSET(out_sample_rate),        AV_OPT_TYPE_INT,    { .i64 = 48000          }, 1,                    INT_MAX,                PARAM },
        { "internal_sample_fmt",    "Internal Sample Format",   OFFSET(internal_sample_fmt),    AV_OPT_TYPE_INT,    { .i64 = AV_SAMPLE_FMT_NONE }, AV_SAMPLE_FMT_NONE,   AV_SAMPLE_FMT_NB-1,     PARAM, "internal_sample_fmt" },
        {"u8" ,  "8-bit unsigned integer",        0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_U8   }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"s16",  "16-bit signed integer",         0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_S16  }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"s32",  "32-bit signed integer",         0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_S32  }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"flt",  "32-bit float",                  0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_FLT  }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"dbl",  "64-bit double",                 0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_DBL  }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"u8p" , "8-bit unsigned integer planar", 0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_U8P  }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"s16p", "16-bit signed integer planar",  0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_S16P }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"s32p", "32-bit signed integer planar",  0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_S32P }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"fltp", "32-bit float planar",           0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_FLTP }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        {"dblp", "64-bit double planar",          0, AV_OPT_TYPE_CONST, {.i64 = AV_SAMPLE_FMT_DBLP }, INT_MIN, INT_MAX, PARAM, "internal_sample_fmt"},
        { "mix_coeff_type",         "Mixing Coefficient Type",  OFFSET(mix_coeff_type),         AV_OPT_TYPE_INT,    { .i64 = AV_MIX_COEFF_TYPE_FLT }, AV_MIX_COEFF_TYPE_Q8, AV_MIX_COEFF_TYPE_NB-1, PARAM, "mix_coeff_type" },
        { "q8",  "16-bit 8.8 Fixed-Point",   0, AV_OPT_TYPE_CONST, { .i64 = AV_MIX_COEFF_TYPE_Q8  }, INT_MIN, INT_MAX, PARAM, "mix_coeff_type" },
        { "q15", "32-bit 17.15 Fixed-Point", 0, AV_OPT_TYPE_CONST, { .i64 = AV_MIX_COEFF_TYPE_Q15 }, INT_MIN, INT_MAX, PARAM, "mix_coeff_type" },
        { "flt", "Floating-Point",           0, AV_OPT_TYPE_CONST, { .i64 = AV_MIX_COEFF_TYPE_FLT }, INT_MIN, INT_MAX, PARAM, "mix_coeff_type" },
        { "center_mix_level",       "Center Mix Level",         OFFSET(center_mix_level),       AV_OPT_TYPE_DOUBLE, { .dbl = M_SQRT1_2      }, -32.0,                32.0,                   PARAM },
        { "surround_mix_level",     "Surround Mix Level",       OFFSET(surround_mix_level),     AV_OPT_TYPE_DOUBLE, { .dbl = M_SQRT1_2      }, -32.0,                32.0,                   PARAM },
        { "lfe_mix_level",          "LFE Mix Level",            OFFSET(lfe_mix_level),          AV_OPT_TYPE_DOUBLE, { .dbl = 0.0            }, -32.0,                32.0,                   PARAM },
        { "normalize_mix_level",    "Normalize Mix Level",      OFFSET(normalize_mix_level),    AV_OPT_TYPE_INT,    { .i64 = 1              }, 0,                    1,                      PARAM },
        { "force_resampling",       "Force Resampling",         OFFSET(force_resampling),       AV_OPT_TYPE_INT,    { .i64 = 0              }, 0,                    1,                      PARAM },
        { "filter_size",            "Resampling Filter Size",   OFFSET(filter_size),            AV_OPT_TYPE_INT,    { .i64 = 16             }, 0,                    32, /* ??? */           PARAM },
        { "phase_shift",            "Resampling Phase Shift",   OFFSET(phase_shift),            AV_OPT_TYPE_INT,    { .i64 = 10             }, 0,                    30, /* ??? */           PARAM },
        { "linear_interp",          "Use Linear Interpolation", OFFSET(linear_interp),          AV_OPT_TYPE_INT,    { .i64 = 0              }, 0,                    1,                      PARAM },
        { "cutoff",                 "Cutoff Frequency Ratio",   OFFSET(cutoff),                 AV_OPT_TYPE_DOUBLE, { .dbl = 0.8            }, 0.0,                  1.0,                    PARAM },
        /* duplicate option in order to work with avconv */
        { "resample_cutoff",        "Cutoff Frequency Ratio",   OFFSET(cutoff),                 AV_OPT_TYPE_DOUBLE, { .dbl = 0.8            }, 0.0,                  1.0,                    PARAM },
        { "matrix_encoding",        "Matrixed Stereo Encoding", OFFSET(matrix_encoding),        AV_OPT_TYPE_INT,    {.i64 =  AV_MATRIX_ENCODING_NONE}, AV_MATRIX_ENCODING_NONE,     AV_MATRIX_ENCODING_NB-1, PARAM, "matrix_encoding" },
        { "none",  "None",               0, AV_OPT_TYPE_CONST, { .i64 = AV_MATRIX_ENCODING_NONE  }, INT_MIN, INT_MAX, PARAM, "matrix_encoding" },
        { "dolby", "Dolby",              0, AV_OPT_TYPE_CONST, { .i64 = AV_MATRIX_ENCODING_DOLBY }, INT_MIN, INT_MAX, PARAM, "matrix_encoding" },
        { "dplii", "Dolby Pro Logic II", 0, AV_OPT_TYPE_CONST, { .i64 = AV_MATRIX_ENCODING_DPLII }, INT_MIN, INT_MAX, PARAM, "matrix_encoding" },
        { "filter_type",            "Filter Type",              OFFSET(filter_type),            AV_OPT_TYPE_INT,    { .i64 = AV_RESAMPLE_FILTER_TYPE_KAISER }, AV_RESAMPLE_FILTER_TYPE_CUBIC, AV_RESAMPLE_FILTER_TYPE_KAISER,  PARAM, "filter_type" },
        { "cubic",            "Cubic",                          0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_FILTER_TYPE_CUBIC            }, INT_MIN, INT_MAX, PARAM, "filter_type" },
        { "blackman_nuttall", "Blackman Nuttall Windowed Sinc", 0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_FILTER_TYPE_BLACKMAN_NUTTALL }, INT_MIN, INT_MAX, PARAM, "filter_type" },
        { "kaiser",           "Kaiser Windowed Sinc",           0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_FILTER_TYPE_KAISER           }, INT_MIN, INT_MAX, PARAM, "filter_type" },
        { "kaiser_beta",            "Kaiser Window Beta",       OFFSET(kaiser_beta),            AV_OPT_TYPE_INT,    { .i64 = 9              }, 2,                    16,                     PARAM },
        { "dither_method",          "Dither Method",            OFFSET(dither_method),          AV_OPT_TYPE_INT,    { .i64 = AV_RESAMPLE_DITHER_NONE }, 0, AV_RESAMPLE_DITHER_NB-1, PARAM, "dither_method"},
        {"none",          "No Dithering",                         0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_DITHER_NONE          }, INT_MIN, INT_MAX, PARAM, "dither_method"},
        {"rectangular",   "Rectangular Dither",                   0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_DITHER_RECTANGULAR   }, INT_MIN, INT_MAX, PARAM, "dither_method"},
        {"triangular",    "Triangular Dither",                    0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_DITHER_TRIANGULAR    }, INT_MIN, INT_MAX, PARAM, "dither_method"},
        {"triangular_hp", "Triangular Dither With High Pass",     0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_DITHER_TRIANGULAR_HP }, INT_MIN, INT_MAX, PARAM, "dither_method"},
        {"triangular_ns", "Triangular Dither With Noise Shaping", 0, AV_OPT_TYPE_CONST, { .i64 = AV_RESAMPLE_DITHER_TRIANGULAR_NS }, INT_MIN, INT_MAX, PARAM, "dither_method"},
        { NULL },
};

const AVClass av_resample_context_class = {
        .class_name = "AVAudioResampleContext",
        .item_name  = av_default_item_name,
        .option     = avresample_options,
        .version    = LIBAVUTIL_VERSION_INT,
};

AVAudioResampleContext *avresample_alloc_context(void)
{
    AVAudioResampleContext *avr;

    avr = av_mallocz(sizeof(*avr));
    if (!avr)
        return NULL;

    avr->av_class = &av_resample_context_class;
    av_opt_set_defaults(avr);

    return avr;
}

void show_help_options(const OptionDefinition *options, const char *msg, int req_flags, int rej_flags, int alt_flags)
{
    const OptionDefinition *po;
    int first;

    first = 1;
    for (po = options; po->name; po++) {
        char buf[64];

        if (((po->flags & req_flags) != req_flags) ||
            (alt_flags && !(po->flags & alt_flags)) ||
            (po->flags & rej_flags))
            continue;

        if (first) {
            printf("%s\n", msg);
            first = 0;
        }
        av_strlcpy(buf, po->name, sizeof(buf));
        if (po->argname) {
            av_strlcat(buf, " ", sizeof(buf));
            av_strlcat(buf, po->argname, sizeof(buf));
        }
        printf("-%-17s  %s\n", buf, po->help);
    }
    printf("\n");
}

/**
 * Trivial log callback.
 * Only suitable for opt_help and similar since it lacks prefix handling.
 */
void log_callback_help(void* ptr, int level, const char* fmt, va_list vl);

void log_callback_help(void *ptr, int level, const char *fmt, va_list vl)
{
    vfprintf(stdout, fmt, vl);
}


/**
 * Show help for all options with given flags in class and all its
 * children.
 */
void show_help_children(const AVClass *class, int flags);

void show_help_children(const AVClass *class, int flags)
{
    const AVClass *child = NULL;
    if (class->option) {
        av_opt_show2(&class, NULL, flags, 0);
        printf("\n");
    }

    while (child = av_opt_child_class_next(class, child))
        show_help_children(child, flags);
}

void show_help_default(const char *opt, const char *arg)
{
    av_log_set_callback(log_callback_help);
    show_usage();
    show_help_options(program_options, "Main options:", 0, OPT_EXPERT, 0);
    show_help_options(program_options, "Advanced options:", OPT_EXPERT, 0, 0);
    printf("\n");
    show_help_children(avcodec_get_class(), AV_OPT_FLAG_DECODING_PARAM);
    show_help_children(avformat_get_class(), AV_OPT_FLAG_DECODING_PARAM);
#if !CONFIG_AVFILTER
    show_help_children(sws_get_class(), AV_OPT_FLAG_ENCODING_PARAM);
#else
    show_help_children(avfilter_get_class(), AV_OPT_FLAG_FILTERING_PARAM);
#endif
    printf("\nWhile playing:\n"
           "q, ESC              quit\n"
           "f                   toggle full screen\n"
           "p, SPC              pause\n"
           "m                   toggle mute\n"
           "9, 0                decrease and increase volume respectively\n"
           "/, *                decrease and increase volume respectively\n"
           "a                   cycle audio channel in the current program\n"
           "v                   cycle video channel\n"
           "t                   cycle subtitle channel in the current program\n"
           "c                   cycle program\n"
           "w                   cycle video filters or show modes\n"
           "s                   activate frame-step mode\n"
           "left/right          seek backward/forward 10 seconds or to custom interval if -seek_interval is set\n"
           "down/up             seek backward/forward 1 minute\n"
           "page down/page up   seek backward/forward 10 minutes\n"
           "right mouse click   seek to percentage in file corresponding to fraction of width\n"
           "left double-click   toggle full screen\n"
    );
}

#define PRINT_CODEC_SUPPORTED(codec, field, type, list_name, term, get_name) \
    if (codec->field) {                                                      \
        const type *p = codec->field;                                        \
                                                                             \
        printf("    Supported " list_name ":");                              \
        while (*p != term) {                                                 \
            get_name(*p);                                                    \
            printf(" %s", name);                                             \
            p++;                                                             \
        }                                                                    \
        printf("\n");                                                        \
} \

#define GET_PIX_FMT_NAME(pix_fmt)\
    const char *name = av_get_pix_fmt_name(pix_fmt);

#define GET_SAMPLE_RATE_NAME(rate)\
    char name[16];\
    snprintf(name, sizeof(name), "%d", rate);

#define GET_CODEC_NAME(id)\
    const char *name = avcodec_descriptor_get(id)->name;

#define GET_SAMPLE_FMT_NAME(sample_fmt)\
    const char *name = av_get_sample_fmt_name(sample_fmt)

#define GET_SAMPLE_RATE_NAME(rate)\
    char name[16];\
    snprintf(name, sizeof(name), "%d", rate);

#define GET_CH_LAYOUT_NAME(ch_layout)\
    char name[16];\
    snprintf(name, sizeof(name), "0x%"PRIx64, ch_layout);

#define GET_CH_LAYOUT_DESC(ch_layout)\
    char name[128];\
    av_get_channel_layout_string(name, sizeof(name), 0, ch_layout);

void print_codec(const AVCodec *c)
{
    int encoder = av_codec_is_encoder(c);

    printf("%s %s [%s]:\n", encoder ? "Encoder" : "Decoder", c->name,
           c->long_name ? c->long_name : "");

    printf("    General capabilities: ");

    if (c->capabilities & AV_CODEC_CAP_DRAW_HORIZ_BAND)
    {
        printf("horizband ");
    }

    if (c->capabilities & AV_CODEC_CAP_DR1)
    {
        printf("dr1 ");
    }

    if (c->capabilities & AV_CODEC_CAP_TRUNCATED)
    {
        printf("trunc ");
    }

    if (c->capabilities & AV_CODEC_CAP_DELAY)
    {
        printf("delay ");
    }

    if (c->capabilities & AV_CODEC_CAP_SMALL_LAST_FRAME)
    {
        printf("small ");
    }

    if (c->capabilities & AV_CODEC_CAP_SUBFRAMES)
    {
        printf("subframes ");
    }

    if (c->capabilities & AV_CODEC_CAP_EXPERIMENTAL)
    {
        printf("exp ");
    }

    if (c->capabilities & AV_CODEC_CAP_CHANNEL_CONF)
    {
        printf("chconf ");
    }

    if (c->capabilities & AV_CODEC_CAP_PARAM_CHANGE)
    {
        printf("paramchange ");
    }

    if (c->capabilities & AV_CODEC_CAP_VARIABLE_FRAME_SIZE)
    {
        printf("variable ");
    }

    if (c->capabilities & (AV_CODEC_CAP_FRAME_THREADS |
                           AV_CODEC_CAP_SLICE_THREADS |
                           AV_CODEC_CAP_AUTO_THREADS))
    {
        printf("threads ");
    }

    if (c->capabilities & AV_CODEC_CAP_AVOID_PROBING)
    {
        printf("avoidprobe ");
    }

    if (c->capabilities & AV_CODEC_CAP_INTRA_ONLY)
    {
        printf("intraonly ");
    }

    if (c->capabilities & AV_CODEC_CAP_LOSSLESS)
    {
        printf("lossless ");
    }

    if (c->capabilities & AV_CODEC_CAP_HARDWARE)
    {
        printf("hardware ");
    }

    if (c->capabilities & AV_CODEC_CAP_HYBRID)
    {
        printf("hybrid ");
    }

    if (!c->capabilities)
    {
        printf("none");
    }

    printf("\n");

    if (c->type == AVMEDIA_TYPE_VIDEO || c->type == AVMEDIA_TYPE_AUDIO)
    {
        printf("    Threading capabilities: ");

        switch (c->capabilities & (AV_CODEC_CAP_FRAME_THREADS |
                                   AV_CODEC_CAP_SLICE_THREADS |
                                   AV_CODEC_CAP_AUTO_THREADS))
        {
            case AV_CODEC_CAP_FRAME_THREADS |
                 AV_CODEC_CAP_SLICE_THREADS:
            {
                printf("frame and slice");
            }
            break;

            case AV_CODEC_CAP_FRAME_THREADS:
            {
                printf("frame");
            }
            break;

            case AV_CODEC_CAP_SLICE_THREADS:
            {
                printf("slice");
            }
            break;

            case AV_CODEC_CAP_AUTO_THREADS:
            {
                printf("auto");
            }
            break;

            default:
            {
                printf("none");
            }
            break;
        }

        printf("\n");
    }

    if (avcodec_get_hw_config(c, 0))
    {
        printf("    Supported hardware devices: ");

        for (int i = 0;; i++)
        {
            const AVCodecHWConfig *config = avcodec_get_hw_config(c, i);

            if (!config)
            {
                break;
            }

            printf("%s ", av_hwdevice_get_type_name(config->device_type));
        }
        printf("\n");
    }

    if (c->supported_framerates)
    {
        const AVRational *fps = c->supported_framerates;

        printf("    Supported framerates:");

        while (fps->num)
        {
            printf(" %d/%d", fps->num, fps->den);
            fps++;
        }

        printf("\n");
    }

    PRINT_CODEC_SUPPORTED(c, pix_fmts, enum AVPixelFormat, "pixel formats",
            AV_PIX_FMT_NONE, GET_PIX_FMT_NAME);

    PRINT_CODEC_SUPPORTED(c, supported_samplerates, int, "sample rates", 0,
            GET_SAMPLE_RATE_NAME);

    PRINT_CODEC_SUPPORTED(c, sample_fmts, enum AVSampleFormat, "sample formats",
            AV_SAMPLE_FMT_NONE, GET_SAMPLE_FMT_NAME);

    PRINT_CODEC_SUPPORTED(c, channel_layouts, uint64_t, "channel layouts",
                          0, GET_CH_LAYOUT_DESC);

    if (c->priv_class)
    {
        show_help_children(c->priv_class,
                           AV_OPT_FLAG_ENCODING_PARAM |
                           AV_OPT_FLAG_DECODING_PARAM);
    }
}

const AVCodec *next_codec_for_id(enum AVCodecID id, const AVCodec *prev, int encoder)
{
    while ((prev = av_codec_next(prev)))
    {
        if (prev->id == id && (encoder ? av_codec_is_encoder(prev) : av_codec_is_decoder(prev)))
        {
            return prev;
        }
    }

    return NULL;
}

void show_help_codec(const char *name, int encoder)
{
    const AVCodecDescriptor *desc;
    const AVCodec *codec;

    if (!name)
    {
        av_log(NULL, AV_LOG_ERROR, "No codec name specified.\n");
        return;
    }

    codec = encoder ? avcodec_find_encoder_by_name(name) :
            avcodec_find_decoder_by_name(name);

    if (codec)
    {
        print_codec(codec);
    }
    else if ((desc = avcodec_descriptor_get_by_name(name)))
    {
        int printed = 0;

        while ((codec = next_codec_for_id(desc->id, codec, encoder)))
        {
            printed = 1;
            print_codec(codec);
        }

        if (!printed)
        {
            av_log(NULL, AV_LOG_ERROR, "Codec '%s' is known to FFmpeg, "
                                       "but no %s for it are available. FFmpeg might need to be "
                                       "recompiled with additional external libraries.\n",
                   name, encoder ? "encoders" : "decoders");
        }
    }
    else
    {
        av_log(NULL, AV_LOG_ERROR, "Codec '%s' is not recognized by FFmpeg.\n", name);
    }
}

void show_help_demuxer(const char *name)
{
    const AVInputFormat *fmt = av_find_input_format(name);

    if (!fmt)
    {
        av_log(NULL, AV_LOG_ERROR, "Unknown format '%s'.\n", name);
        return;
    }

    printf("Demuxer %s [%s]:\n", fmt->name, fmt->long_name);

    if (fmt->extensions)
    {
        printf("    Common extensions: %s.\n", fmt->extensions);
    }

    if (fmt->priv_class)
    {
        show_help_children(fmt->priv_class, AV_OPT_FLAG_DECODING_PARAM);
    }
}

void show_help_muxer(const char *name)
{
    const AVCodecDescriptor *desc;
    const AVOutputFormat *fmt = av_guess_format(name, NULL, NULL);

    if (!fmt)
    {
        av_log(NULL, AV_LOG_ERROR, "Unknown format '%s'.\n", name);
        return;
    }

    printf("Muxer %s [%s]:\n", fmt->name, fmt->long_name);

    if (fmt->extensions)
    {
        printf("    Common extensions: %s.\n", fmt->extensions);
    }

    if (fmt->mime_type)
    {
        printf("    Mime type: %s.\n", fmt->mime_type);
    }

    if (fmt->video_codec != AV_CODEC_ID_NONE &&
        (desc = avcodec_descriptor_get(fmt->video_codec)))
    {
        printf("    Default video codec: %s.\n", desc->name);
    }

    if (fmt->audio_codec != AV_CODEC_ID_NONE &&
        (desc = avcodec_descriptor_get(fmt->audio_codec)))
    {
        printf("    Default audio codec: %s.\n", desc->name);
    }

    if (fmt->subtitle_codec != AV_CODEC_ID_NONE &&
        (desc = avcodec_descriptor_get(fmt->subtitle_codec)))
    {
        printf("    Default subtitle codec: %s.\n", desc->name);
    }

    if (fmt->priv_class)
    {
        show_help_children(fmt->priv_class, AV_OPT_FLAG_ENCODING_PARAM);
    }
}

#define media_type_string av_get_media_type_string

void show_help_filter(const char *name)
{
    const AVFilter *f = avfilter_get_by_name(name);
    int i, count;

    if (!name)
    {
        av_log(NULL, AV_LOG_ERROR, "No filter name specified.\n");
        return;
    }
    else if (!f)
    {
        av_log(NULL, AV_LOG_ERROR, "Unknown filter '%s'.\n", name);
        return;
    }

    printf("Filter %s\n", f->name);

    if (f->description)
    {
        printf("  %s\n", f->description);
    }

    if (f->flags & AVFILTER_FLAG_SLICE_THREADS)
    {
        printf("    slice threading supported\n");
    }

    printf("    Inputs:\n");
    count = avfilter_pad_count(f->inputs);

    for (i = 0; i < count; i++)
    {
        printf("       #%d: %s (%s)\n", i, avfilter_pad_get_name(f->inputs, i),
               media_type_string(avfilter_pad_get_type(f->inputs, i)));
    }

    if (f->flags & AVFILTER_FLAG_DYNAMIC_INPUTS)
    {
        printf("        dynamic (depending on the options)\n");
    }
    else if (!count)
    {
        printf("        none (source filter)\n");
    }

    printf("    Outputs:\n");
    count = avfilter_pad_count(f->outputs);

    for (i = 0; i < count; i++)
    {
        printf("       #%d: %s (%s)\n", i, avfilter_pad_get_name(f->outputs, i), media_type_string(avfilter_pad_get_type(f->outputs, i)));
    }

    if (f->flags & AVFILTER_FLAG_DYNAMIC_OUTPUTS)
    {
        printf("        dynamic (depending on the options)\n");
    }
    else if (!count)
    {
        printf("        none (sink filter)\n");
    }

    if (f->priv_class)
    {
        show_help_children(f->priv_class, AV_OPT_FLAG_VIDEO_PARAM | AV_OPT_FLAG_FILTERING_PARAM |
                                          AV_OPT_FLAG_AUDIO_PARAM);
    }

    if (f->flags & AVFILTER_FLAG_SUPPORT_TIMELINE)
    {
        printf("This filter has support for timeline through the 'enable' option.\n");
    }
}

void show_help_bsf(const char *name)
{
    const AVBitStreamFilter *bsf = av_bsf_get_by_name(name);

    if (!name)
    {
        av_log(NULL, AV_LOG_ERROR, "No bitstream filter name specified.\n");
        return;
    }
    else if (!bsf)
    {
        av_log(NULL, AV_LOG_ERROR, "Unknown bit stream filter '%s'.\n", name);
        return;
    }

    printf("Bit stream filter %s\n", bsf->name);
    PRINT_CODEC_SUPPORTED(bsf, codec_ids, enum AVCodecID, "codecs",
                          AV_CODEC_ID_NONE, GET_CODEC_NAME);
    if (bsf->priv_class)
    {
        show_help_children(bsf->priv_class, AV_OPT_FLAG_BSF_PARAM);
    }
}

int show_help(void *optctx, const char *opt, const char *arg)
{
    char *topic, *par;
    av_log_set_callback(log_callback_help);

    topic = av_strdup(arg ? arg : "");

    if (!topic)
    {
        return AVERROR(ENOMEM);
    }

    par = strchr(topic, '=');

    if (par)
    {
        *par++ = 0;
    }

    if (!*topic)
    {
        show_help_default(topic, par);
    }
    else if (!strcmp(topic, "decoder"))
    {
        show_help_codec(par, 0);
    }
    else if (!strcmp(topic, "encoder"))
    {
        show_help_codec(par, 1);
    }
    else if (!strcmp(topic, "demuxer"))
    {
        show_help_demuxer(par);
    }
    else if (!strcmp(topic, "muxer"))
    {
        show_help_muxer(par);
    }
    else if (!strcmp(topic, "filter"))
    {
        show_help_filter(par);
    }
    else if (!strcmp(topic, "bsf"))
    {
        show_help_bsf(par);
    }
    else
    {
        show_help_default(topic, par);
    }

    av_freep(&topic);
    return 0;
}

void print_program_info(int flags, int level)
{
    // indent using 2 spaces if the INDENT flag is set
    const char *indent = flags & INDENT? "  " : "";

    // print program name and version
    av_log(NULL, level, "%s version %s\n", program_name, FFPLAYMOTION_VERSION);

    // if the copyright message has to be shown
    if (flags & SHOW_COPYRIGHT)
    {
        // print copyright message
        av_log(NULL, level, "ffmpeg: Copyright (c) %d-%d the FFmpeg developers\n", ffplay_birth_year, CONFIG_THIS_YEAR);
        av_log(NULL, level, "player: Copyright (c) %d-%d Rambod Rahmani\n", player_birth_year, CONFIG_THIS_YEAR);
        av_log(NULL, level, "ffmotionplay: Copyright (c) %d-%d Eduard Minasyan\n", motion_birth_year, CONFIG_THIS_YEAR);
    }

    // print gcc information message followed by a new line
    av_log(NULL, level, "%sbuilt with %s\n", indent, CC_IDENT);

    // print ffmpeg configuration message
    av_log(NULL, level, "%sconfiguration: " FFMPEG_CONFIGURATION "\n", indent);
}

/**
* Return the libpostproc build-time configuration.
*/
const char *postproc_configuration(void);

const char *postproc_configuration(void)
{
    return FFMPEG_CONFIGURATION;
}


/**
 * Return the LIBPOSTPROC_VERSION_INT constant.
 */
unsigned postproc_version(void);

unsigned postproc_version(void)
{
    av_assert0(LIBPOSTPROC_VERSION_MICRO >= 100);
    return LIBPOSTPROC_VERSION_INT;
}

const char *avresample_configuration(void)
{
    return FFMPEG_CONFIGURATION;
}

unsigned avresample_version(void)
{
    return LIBAVRESAMPLE_VERSION_INT;
}

void print_all_libs_info(int flags, int level)
{
    PRINT_LIB_INFO(avutil,     AVUTIL,     flags, level);
    PRINT_LIB_INFO(avcodec,    AVCODEC,    flags, level);
    PRINT_LIB_INFO(avformat,   AVFORMAT,   flags, level);
    PRINT_LIB_INFO(avdevice,   AVDEVICE,   flags, level);
    PRINT_LIB_INFO(avfilter,   AVFILTER,   flags, level);
    PRINT_LIB_INFO(avresample, AVRESAMPLE, flags, level);
    PRINT_LIB_INFO(swscale,    SWSCALE,    flags, level);
    PRINT_LIB_INFO(swresample, SWRESAMPLE, flags, level);
    PRINT_LIB_INFO(postproc,   POSTPROC,   flags, level);
}

int show_version(void *optctx, const char *opt, const char *arg)
{
    av_log_set_callback(log_callback_help);
    print_program_info(SHOW_COPYRIGHT, AV_LOG_INFO);
    print_all_libs_info(SHOW_VERSION, AV_LOG_INFO);

    return 0;
}

void print_buildconf(int flags, int level)
{
    const char *indent = flags & INDENT ? "  " : "";
    char str[] = { FFMPEG_CONFIGURATION };
    char *conflist, *remove_tilde, *splitconf;

    // Change all the ' --' strings to '~--' so that
    // they can be identified as tokens.
    while ((conflist = strstr(str, " --")) != NULL)
    {
        strncpy(conflist, "~--", 3);
    }

    // Compensate for the weirdness this would cause
    // when passing 'pkg-config --static'.
    while ((remove_tilde = strstr(str, "pkg-config~")) != NULL)
    {
        strncpy(remove_tilde, "pkg-config ", 11);
    }

    splitconf = strtok(str, "~");
    av_log(NULL, level, "\n%sconfiguration:\n", indent);

    while (splitconf != NULL)
    {
        av_log(NULL, level, "%s%s%s\n", indent, indent, splitconf);
        splitconf = strtok(NULL, "~");
    }
}

int show_buildconf(void *optctx, const char *opt, const char *arg)
{
    av_log_set_callback(log_callback_help);
    print_buildconf(INDENT|0, AV_LOG_INFO);

    return 0;
}

int is_device(const AVClass *avclass)
{
    if (!avclass)
    {
        return 0;
    }

    return AV_IS_INPUT_DEVICE(avclass->category) || AV_IS_OUTPUT_DEVICE(avclass->category);
}

int show_formats_devices(void *optctx, const char *opt, const char *arg, int device_only, int muxdemuxers)
{
    void *ifmt_opaque = NULL;
    const AVInputFormat *ifmt  = NULL;
    void *ofmt_opaque = NULL;
    const AVOutputFormat *ofmt = NULL;
    const char *last_name;
    int is_dev;

    printf("%s\n"
           " D. = Demuxing supported\n"
           " .E = Muxing supported\n"
           " --\n", device_only ? "Devices:" : "File formats:");

    last_name = "000";

    for (;;)
    {
        int decode = 0;
        int encode = 0;
        const char *name      = NULL;
        const char *long_name = NULL;

        if (muxdemuxers !=SHOW_DEMUXERS)
        {
            ofmt_opaque = NULL;

            while ((ofmt = av_muxer_iterate(&ofmt_opaque)))
            {
                is_dev = is_device(ofmt->priv_class);

                if (!is_dev && device_only)
                {
                    continue;
                }

                if ((!name || strcmp(ofmt->name, name) < 0) && strcmp(ofmt->name, last_name) > 0)
                {
                    name      = ofmt->name;
                    long_name = ofmt->long_name;
                    encode    = 1;
                }
            }
        }

        if (muxdemuxers != SHOW_MUXERS)
        {
            ifmt_opaque = NULL;

            while ((ifmt = av_demuxer_iterate(&ifmt_opaque)))
            {
                is_dev = is_device(ifmt->priv_class);

                if (!is_dev && device_only)
                {
                    continue;
                }

                if ((!name || strcmp(ifmt->name, name) < 0) &&
                    strcmp(ifmt->name, last_name) > 0)
                {
                    name      = ifmt->name;
                    long_name = ifmt->long_name;
                    encode    = 0;
                }
                if (name && strcmp(ifmt->name, name) == 0)
                    decode = 1;
            }
        }

        if (!name)
        {
            break;
        }

        last_name = name;

        printf(" %s%s %-15s %s\n",
               decode ? "D" : " ",
               encode ? "E" : " ",
               name,
               long_name ? long_name:" ");
    }
    return 0;
}

int show_formats(void *optctx, const char *opt, const char *arg)
{
    return show_formats_devices(optctx, opt, arg, 0, SHOW_DEFAULT);
}

int show_muxers(void *optctx, const char *opt, const char *arg)
{
    return show_formats_devices(optctx, opt, arg, 0, SHOW_MUXERS);
}

int show_demuxers(void *optctx, const char *opt, const char *arg)
{
    return show_formats_devices(optctx, opt, arg, 0, SHOW_DEMUXERS);
}

int show_devices(void *optctx, const char *opt, const char *arg)
{
    return show_formats_devices(optctx, opt, arg, 1, SHOW_DEFAULT);
}

char get_media_type_char(enum AVMediaType type)
{
    switch (type)
    {
        case AVMEDIA_TYPE_VIDEO:
        {
            return 'V';
        }

        case AVMEDIA_TYPE_AUDIO:
        {
            return 'A';
        }

        case AVMEDIA_TYPE_DATA:
        {
            return 'D';
        }

        case AVMEDIA_TYPE_SUBTITLE:
        {
            return 'S';
        }

        case AVMEDIA_TYPE_ATTACHMENT:
        {
            return 'T';
        }

        default:
        {
            return '?';
        }
    }
}

int compare_codec_desc(const void *a, const void *b)
{
    const AVCodecDescriptor * const *da = a;
    const AVCodecDescriptor * const *db = b;

    return (*da)->type != (*db)->type ? FFDIFFSIGN((*da)->type, (*db)->type) : strcmp((*da)->name, (*db)->name);
}

unsigned get_codecs_sorted(const AVCodecDescriptor ***rcodecs)
{
    const AVCodecDescriptor *desc = NULL;
    const AVCodecDescriptor **codecs;
    unsigned nb_codecs = 0, i = 0;

    while ((desc = avcodec_descriptor_next(desc)))
    {
        nb_codecs++;
    }

    if (!(codecs = av_calloc(nb_codecs, sizeof(*codecs))))
    {
        av_log(NULL, AV_LOG_ERROR, "Out of memory\n");
        exit_program(1);
    }

    desc = NULL;

    while ((desc = avcodec_descriptor_next(desc)))
    {
        codecs[i++] = desc;
    }

    av_assert0(i == nb_codecs);

    qsort(codecs, nb_codecs, sizeof(*codecs), compare_codec_desc);

    *rcodecs = codecs;

    return nb_codecs;
}

void print_codecs_for_id(enum AVCodecID id, int encoder)
{
    const AVCodec *codec = NULL;

    printf(" (%s: ", encoder ? "encoders" : "decoders");

    while ((codec = next_codec_for_id(id, codec, encoder)))
    {
        printf("%s ", codec->name);
    }

    printf(")");
}

int show_codecs(void *optctx, const char *opt, const char *arg)
{
    const AVCodecDescriptor **codecs;
    unsigned i, nb_codecs = get_codecs_sorted(&codecs);

    printf("Codecs:\n"
           " D..... = Decoding supported\n"
           " .E.... = Encoding supported\n"
           " ..V... = Video codec\n"
           " ..A... = Audio codec\n"
           " ..S... = Subtitle codec\n"
           " ...I.. = Intra frame-only codec\n"
           " ....L. = Lossy compression\n"
           " .....S = Lossless compression\n"
           " -------\n");
    for (i = 0; i < nb_codecs; i++)
    {
        const AVCodecDescriptor *desc = codecs[i];
        const AVCodec *codec = NULL;

        if (strstr(desc->name, "_deprecated"))
        {
            continue;
        }

        printf(" ");
        printf(avcodec_find_decoder(desc->id) ? "D" : ".");
        printf(avcodec_find_encoder(desc->id) ? "E" : ".");

        printf("%c", get_media_type_char(desc->type));
        printf((desc->props & AV_CODEC_PROP_INTRA_ONLY) ? "I" : ".");
        printf((desc->props & AV_CODEC_PROP_LOSSY)      ? "L" : ".");
        printf((desc->props & AV_CODEC_PROP_LOSSLESS)   ? "S" : ".");

        printf(" %-20s %s", desc->name, desc->long_name ? desc->long_name : "");

        /* print decoders/encoders when there's more than one or their
         * names are different from codec name */
        while ((codec = next_codec_for_id(desc->id, codec, 0)))
        {
            if (strcmp(codec->name, desc->name))
            {
                print_codecs_for_id(desc->id, 0);
                break;
            }
        }

        codec = NULL;

        while ((codec = next_codec_for_id(desc->id, codec, 1)))
        {
            if (strcmp(codec->name, desc->name))
            {
                print_codecs_for_id(desc->id, 1);
                break;
            }
        }

        printf("\n");
    }

    av_free(codecs);

    return 0;
}

void print_codecs(int encoder)
{
    const AVCodecDescriptor **codecs;
    unsigned i, nb_codecs = get_codecs_sorted(&codecs);

    printf("%s:\n"
           " V..... = Video\n"
           " A..... = Audio\n"
           " S..... = Subtitle\n"
           " .F.... = Frame-level multithreading\n"
           " ..S... = Slice-level multithreading\n"
           " ...X.. = Codec is experimental\n"
           " ....B. = Supports draw_horiz_band\n"
           " .....D = Supports direct rendering method 1\n"
           " ------\n",
           encoder ? "Encoders" : "Decoders");

    for (i = 0; i < nb_codecs; i++)
    {
        const AVCodecDescriptor *desc = codecs[i];
        const AVCodec *codec = NULL;

        while ((codec = next_codec_for_id(desc->id, codec, encoder)))
        {
            printf(" %c", get_media_type_char(desc->type));
            printf((codec->capabilities & AV_CODEC_CAP_FRAME_THREADS) ? "F" : ".");
            printf((codec->capabilities & AV_CODEC_CAP_SLICE_THREADS) ? "S" : ".");
            printf((codec->capabilities & AV_CODEC_CAP_EXPERIMENTAL)  ? "X" : ".");
            printf((codec->capabilities & AV_CODEC_CAP_DRAW_HORIZ_BAND)?"B" : ".");
            printf((codec->capabilities & AV_CODEC_CAP_DR1)           ? "D" : ".");

            printf(" %-20s %s", codec->name, codec->long_name ? codec->long_name : "");

            if (strcmp(codec->name, desc->name))
            {
                printf(" (codec %s)", desc->name);
            }

            printf("\n");
        }
    }
    av_free(codecs);
}

int show_decoders(void *optctx, const char *opt, const char *arg)
{
    print_codecs(0);
    return 0;
}

int show_encoders(void *optctx, const char *opt, const char *arg)
{
    print_codecs(1);
    return 0;
}

int show_bsfs(void *optctx, const char *opt, const char *arg)
{
    const AVBitStreamFilter *bsf = NULL;
    void *opaque = NULL;

    printf("Bitstream filters:\n");

    while ((bsf = av_bsf_iterate(&opaque)))
    {
        printf("%s\n", bsf->name);
    }

    printf("\n");

    return 0;
}

int show_protocols(void *optctx, const char *opt, const char *arg)
{
    void *opaque = NULL;
    const char *name;

    printf("Supported file protocols:\n"
           "Input:\n");

    while ((name = avio_enum_protocols(&opaque, 0)))
    {
        printf("  %s\n", name);
    }

    printf("Output:\n");

    while ((name = avio_enum_protocols(&opaque, 1)))
    {
        printf("  %s\n", name);
    }

    return 0;
}

int show_filters(void *optctx, const char *opt, const char *arg)
{
    const AVFilter *filter = NULL;
    char descr[64], *descr_cur;
    void *opaque = NULL;
    int i, j;
    const AVFilterPad *pad;

    printf("Filters:\n"
           "  T.. = Timeline support\n"
           "  .S. = Slice threading\n"
           "  ..C = Command support\n"
           "  A = Audio input/output\n"
           "  V = Video input/output\n"
           "  N = Dynamic number and/or type of input/output\n"
           "  | = Source or sink filter\n");

    while ((filter = av_filter_iterate(&opaque)))
    {
        descr_cur = descr;

        for (i = 0; i < 2; i++)
        {
            if (i)
            {
                *(descr_cur++) = '-';
                *(descr_cur++) = '>';
            }

            pad = i ? filter->outputs : filter->inputs;

            for (j = 0; pad && avfilter_pad_get_name(pad, j); j++)
            {
                if (descr_cur >= descr + sizeof(descr) - 4)
                {
                    break;
                }

                *(descr_cur++) = get_media_type_char(avfilter_pad_get_type(pad, j));
            }

            if (!j)
            {
                *(descr_cur++) = ((!i && (filter->flags & AVFILTER_FLAG_DYNAMIC_INPUTS)) ||
                                  ( i && (filter->flags & AVFILTER_FLAG_DYNAMIC_OUTPUTS))) ? 'N' : '|';
            }
        }

        *descr_cur = 0;
        printf(" %c%c%c %-17s %-10s %s\n",
               filter->flags & AVFILTER_FLAG_SUPPORT_TIMELINE ? 'T' : '.',
               filter->flags & AVFILTER_FLAG_SLICE_THREADS    ? 'S' : '.',
               filter->process_command                        ? 'C' : '.',
               filter->name, descr, filter->description);
    }


    return 0;
}

int show_pix_fmts(void *optctx, const char *opt, const char *arg)
{
    const AVPixFmtDescriptor *pix_desc = NULL;

    printf("Pixel formats:\n"
           "I.... = Supported Input  format for conversion\n"
           ".O... = Supported Output format for conversion\n"
           "..H.. = Hardware accelerated format\n"
           "...P. = Paletted format\n"
           "....B = Bitstream format\n"
           "FLAGS NAME            NB_COMPONENTS BITS_PER_PIXEL\n"
           "-----\n");

#if !CONFIG_SWSCALE
#   define sws_isSupportedInput(x)  0
#   define sws_isSupportedOutput(x) 0
#endif

    while ((pix_desc = av_pix_fmt_desc_next(pix_desc)))
    {
        enum AVPixelFormat av_unused pix_fmt = av_pix_fmt_desc_get_id(pix_desc);
        printf("%c%c%c%c%c %-16s       %d            %2d\n",
               sws_isSupportedInput (pix_fmt)              ? 'I' : '.',
               sws_isSupportedOutput(pix_fmt)              ? 'O' : '.',
               pix_desc->flags & AV_PIX_FMT_FLAG_HWACCEL   ? 'H' : '.',
               pix_desc->flags & AV_PIX_FMT_FLAG_PAL       ? 'P' : '.',
               pix_desc->flags & AV_PIX_FMT_FLAG_BITSTREAM ? 'B' : '.',
               pix_desc->name,
               pix_desc->nb_components,
               av_get_bits_per_pixel(pix_desc));
    }

    return 0;
}

int show_layouts(void *optctx, const char *opt, const char *arg)
{
    int i = 0;
    uint64_t layout, j;
    const char *name, *descr;

    printf("Individual channels:\n"
           "NAME           DESCRIPTION\n");

    for (i = 0; i < 63; i++)
    {
        name = av_get_channel_name((uint64_t)1 << i);
        if (!name)
        {
            continue;
        }

        descr = av_get_channel_description((uint64_t)1 << i);
        printf("%-14s %s\n", name, descr);
    }

    printf("\nStandard channel layouts:\n"
           "NAME           DECOMPOSITION\n");

    for (i = 0; !av_get_standard_channel_layout(i, &layout, &name); i++)
    {
        if (name)
        {
            printf("%-14s ", name);

            for (j = 1; j; j <<= 1)
            {
                if ((layout & j))
                {
                    printf("%s%s", (layout & (j - 1)) ? "+" : "", av_get_channel_name(j));
                }
            }

            printf("\n");
        }
    }

    return 0;
}

int show_sample_fmts(void *optctx, const char *opt, const char *arg)
{
    int i;
    char fmt_str[128];

    for (i = -1; i < AV_SAMPLE_FMT_NB; i++)
    {
        printf("%s\n", av_get_sample_fmt_string(fmt_str, sizeof(fmt_str), i));
    }

    return 0;
}

int show_colors(void *optctx, const char *opt, const char *arg)
{
    const char *name;
    const uint8_t *rgb;
    int i;

    printf("%-32s #RRGGBB\n", "name");

    for (i = 0; name = av_get_known_color_name(i, &rgb); i++)
    {
        printf("%-32s #%02x%02x%02x\n", name, rgb[0], rgb[1], rgb[2]);
    }

    return 0;
}

int opt_loglevel(void *optctx, const char *opt, const char *arg)
{
    const struct { const char *name; int level; } log_levels[] = {
            { "quiet"  , AV_LOG_QUIET   },
            { "panic"  , AV_LOG_PANIC   },
            { "fatal"  , AV_LOG_FATAL   },
            { "error"  , AV_LOG_ERROR   },
            { "warning", AV_LOG_WARNING },
            { "info"   , AV_LOG_INFO    },
            { "verbose", AV_LOG_VERBOSE },
            { "debug"  , AV_LOG_DEBUG   },
            { "trace"  , AV_LOG_TRACE   },
    };

    const char *token;
    char *tail;
    int flags = av_log_get_flags();
    int level = av_log_get_level();
    int cmd, i = 0;

    av_assert0(arg);

    while (*arg)
    {
        token = arg;

        if (*token == '+' || *token == '-')
        {
            cmd = *token++;
        }
        else
        {
            cmd = 0;
        }

        if (!i && !cmd)
        {
            flags = 0;  /* missing relative prefix, build absolute value */
        }

        if (!strncmp(token, "repeat", 6))
        {
            if (cmd == '-')
            {
                flags |= AV_LOG_SKIP_REPEATED;
            }
            else
            {
                flags &= ~AV_LOG_SKIP_REPEATED;
            }

            arg = token + 6;
        }
        else if (!strncmp(token, "level", 5))
        {
            if (cmd == '-')
            {
                flags &= ~AV_LOG_PRINT_LEVEL;
            }
            else
            {
                flags |= AV_LOG_PRINT_LEVEL;
            }

            arg = token + 5;
        }
        else
        {
            break;
        }

        i++;
    }

    if (!*arg)
    {
        goto end;
    }
    else if (*arg == '+')
    {
        arg++;
    }
    else if (!i)
    {
        flags = av_log_get_flags();  /* level value without prefix, reset flags */
    }

    for (i = 0; i < FF_ARRAY_ELEMS(log_levels); i++)
    {
        if (!strcmp(log_levels[i].name, arg))
        {
            level = log_levels[i].level;
            goto end;
        }
    }

    level = strtol(arg, &tail, 10);

    if (*tail)
    {
        av_log(NULL, AV_LOG_FATAL, "Invalid loglevel \"%s\". "
                                   "Possible levels are numbers or:\n", arg);

        for (i = 0; i < FF_ARRAY_ELEMS(log_levels); i++)
        {
            av_log(NULL, AV_LOG_FATAL, "\"%s\"\n", log_levels[i].name);
        }

        exit_program(1);
    }

    end:
        av_log_set_flags(flags);
        av_log_set_level(level);
        return 0;
}

void log_callback_report(void *ptr, int level, const char *fmt, va_list vl)
{
    va_list vl2;
    char line[1024];
    static int print_prefix = 1;

    va_copy(vl2, vl);
    av_log_default_callback(ptr, level, fmt, vl);
    av_log_format_line(ptr, level, fmt, vl2, line, sizeof(line), &print_prefix);
    va_end(vl2);

    if (report_file_level >= level)
    {
        fputs(line, report_file);
        fflush(report_file);
    }
}

void expand_filename_template(AVBPrint *bp, const char *template, struct tm *tm)
{
    int c;

    while ((c = *(template++)))
    {
        if (c == '%')
        {
            if (!(c = *(template++)))
            {
                break;
            }

            switch (c)
            {
                case 'p':
                {
                    av_bprintf(bp, "%s", program_name);
                }
                break;

                case 't':
                {
                    av_bprintf(bp, "%04d%02d%02d-%02d%02d%02d",
                               tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
                               tm->tm_hour, tm->tm_min, tm->tm_sec);
                }
                break;

                case '%':
                {
                    av_bprint_chars(bp, c, 1);
                }
                break;

                default:
                {
                    // nothing to do
                }
                break;
            }
        }
        else
        {
            av_bprint_chars(bp, c, 1);
        }
    }
}

/**
 *
 * @param env
 * @return
 */
int init_report(const char *env);

int init_report(const char *env)
{
    char *filename_template = NULL;
    char *key, *val;
    int ret, count = 0;
    time_t now;
    struct tm *tm;
    AVBPrint filename;

    // report file already opened
    if (report_file)
    {
        return 0;
    }

    // get the current time and put it in now
    time(&now);

    // get the representation of now in the local timezone
    tm = localtime(&now);

    while (env && *env)
    {
        if ((ret = av_opt_get_key_value(&env, "=", ":", 0, &key, &val)) < 0)
        {
            if (count)
            {
                av_log(NULL, AV_LOG_ERROR, "Failed to parse FFREPORT environment variable: %s\n", av_err2str(ret));
            }
            break;
        }

        if (*env)
        {
            env++;
        }

        count++;

        if (!strcmp(key, "file"))
        {
            av_free(filename_template);
            filename_template = val;
            val = NULL;
        }
        else if (!strcmp(key, "level"))
        {
            char *tail;
            report_file_level = strtol(val, &tail, 10);

            if (*tail)
            {
                av_log(NULL, AV_LOG_FATAL, "Invalid report file level\n");
                exit_program(1);
            }
        }
        else
        {
            av_log(NULL, AV_LOG_ERROR, "Unknown key '%s' in FFREPORT\n", key);
        }

        av_free(val);
        av_free(key);
    }

    av_bprint_init(&filename, 0, AV_BPRINT_SIZE_AUTOMATIC);

    expand_filename_template(&filename, av_x_if_null(filename_template, "%p-%t.log"), tm);

    av_free(filename_template);

    if (!av_bprint_is_complete(&filename))
    {
        av_log(NULL, AV_LOG_ERROR, "Out of memory building report file name\n");
        return AVERROR(ENOMEM);
    }

    report_file = fopen(filename.str, "w");

    if (!report_file)
    {
        int ret = AVERROR(errno);

        av_log(NULL, AV_LOG_ERROR, "Failed to open report \"%s\": %s\n", filename.str, strerror(errno));

        return ret;
    }

    av_log_set_callback(log_callback_report);

    av_log(NULL, AV_LOG_INFO,
           "%s started on %04d-%02d-%02d at %02d:%02d:%02d\n"
           "Report written to \"%s\"\n",
           program_name,
           tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
           tm->tm_hour, tm->tm_min, tm->tm_sec,
           filename.str);

    av_bprint_finalize(&filename, NULL);

    return 0;
}

int opt_report(const char *opt)
{
    return init_report(NULL);
}

int opt_max_alloc(void *optctx, const char *opt, const char *arg)
{
    char *tail;
    size_t max;

    max = strtol(arg, &tail, 10);

    if (*tail)
    {
        av_log(NULL, AV_LOG_FATAL, "Invalid max_alloc \"%s\".\n", arg);
        exit_program(1);
    }

    av_max_alloc(max);

    return 0;
}

int opt_cpuflags(void *optctx, const char *opt, const char *arg)
{
    int ret;
    unsigned flags = av_get_cpu_flags();

    if ((ret = av_parse_cpu_caps(&flags, arg)) < 0)
    {
        return ret;
    }

    av_force_cpu_flags(flags);

    return 0;
}

void init_dynload(void)
{
    #ifdef _WIN32
        /* Calling SetDllDirectory with the empty string (but not NULL) removes the
         * current working directory from the DLL search path as a security pre-caution. */
        SetDllDirectory("");
    #endif
}

const OptionDefinition *find_option(const OptionDefinition * options, const char *name)
{
    // find the first occurrence of ':'
    const char *p = strchr(name, ':');

    int len = p ? p - name : strlen(name);

    while (options->name)
    {
        if (!strncmp(name, options->name, len) && strlen(options->name) == len)
        {
            break;
        }

        options++;
    }

    return options;
}

int locate_option(int argc, char **argv, const OptionDefinition *options, const char *option_name)
{
    const OptionDefinition *po;

    // current command line option index
    int i;

    // loop through the given command line options (argv[])
    for (i = 1; i < argc; i++)
    {
        // start with the first command line option, argv[0] is the program name
        const char *cur_opt = argv[i];

        // if it does not start with '-' then it is not a command line option
        if (*cur_opt++ != '-')
        {
            // skip
            continue;
        }

        // find the option definition for the current option
        po = find_option(options, cur_opt);

        if (!po->name && cur_opt[0] == 'n' && cur_opt[1] == 'o')
        {
            po = find_option(options, cur_opt + 2);
        }

        // triple check the given option name with the one found in the command line arguments and in the options definition list
        if ((!po->name && !strcmp(cur_opt, option_name)) || (po->name && !strcmp(option_name, po->name)))
        {
            // return the option index
            return i;
        }

        if (!po->name || po->flags & HAS_ARG)
        {
            i++;
        }
    }

    // option not found
    return 0;
}

void check_options(const OptionDefinition * options)
{
    while (options->name)
    {
        if (options->flags & OPT_PERFILE)
        {
            av_assert0(options->flags & (OPT_INPUT | OPT_OUTPUT));
        }

        options++;
    }
}

void dump_argument(const char *a)
{
    const unsigned char *p;

    for (p = a; *p; p++)
    {
        if (!((*p >= '+' && *p <= ':') || (*p >= '@' && *p <= 'Z') || *p == '_' || (*p >= 'a' && *p <= 'z')))
        {
            break;
        }
    }

    if (!*p)
    {
        fputs(a, report_file);
        return;
    }

    fputc('"', report_file);

    for (p = a; *p; p++)
    {
        if (*p == '\\' || *p == '"' || *p == '$' || *p == '`')
            fprintf(report_file, "\\%c", *p);
        else if (*p < ' ' || *p > '~')
            fprintf(report_file, "\\x%02x", *p);
        else
            fputc(*p, report_file);
    }

    fputc('"', report_file);
}

void parse_loglevel(int argc, char **argv, const OptionDefinition *options)
{
    // locate the -loglevel option index in the given command line arguments
    int idx = locate_option(argc, argv, options, "loglevel");

    //
    check_options(options);

    // if -loglevel option was not found
    if (!idx)
    {
        // search for -v option index
        idx = locate_option(argc, argv, options, "v");
    }

    // if either -loglevel or -v was found
    if (idx && argv[idx + 1])
    {
        opt_loglevel(NULL, "loglevel", argv[idx + 1]);
    }

    // locate the -report option index
    idx = locate_option(argc, argv, options, "report");

    //
    const char *env;

    // setting the environment variable FFREPORT to any value has the same effect
    if ((env = getenv("FFREPORT")) || idx)
    {
        init_report(env);

        if (report_file)
        {
            int i;

            fprintf(report_file, "Command line:\n");

            for (i = 0; i < argc; i++)
            {
                dump_argument(argv[i]);
                fputc(i < argc - 1 ? ' ' : '\n', report_file);
            }

            fflush(report_file);
        }
    }

    // locate the -hide_banner option index
    idx = locate_option(argc, argv, options, "hide_banner");

    // set the hide_banner if the -hide_banner option was found
    if (idx)
    {
        hide_banner = 1;
    }
}

void show_banner(int argc, char **argv, const OptionDefinition *options)
{
    // locate the -version option index
    int idx = locate_option(argc, argv, options, "version");

    // if either the -hide_banner or -version command line options are set
    if (hide_banner || idx)
    {
        // return
        return;
    }

    // print indented program inforamtion
    print_program_info(INDENT|SHOW_COPYRIGHT, AV_LOG_INFO);

    // print all enabled libraries configuration
    print_all_libs_info(INDENT|SHOW_CONFIG,  AV_LOG_INFO);

    // print all enabled libraries version
    print_all_libs_info(INDENT|SHOW_VERSION, AV_LOG_INFO);
}

void prepare_app_arguments(int *argc_ptr, char ***argv_ptr);
/* _WIN32 means using the windows libc - cygwin doesn't define that
 * by default. HAVE_COMMANDLINETOARGVW is true on cygwin, while
 * it doesn't provide the actual command line via GetCommandLineW(). */
#if HAVE_COMMANDLINETOARGVW && defined(_WIN32)
#include <shellapi.h>
/* Will be leaked on exit */
char** win32_argv_utf8 = NULL;
int win32_argc = 0;

/**
 * Prepare command line arguments for executable.
 * For Windows - perform wide-char to UTF-8 conversion.
 * Input arguments should be main() function arguments.
 * @param argc_ptr Arguments number (including executable)
 * @param argv_ptr Arguments list.
 */
void prepare_app_arguments(int *argc_ptr, char ***argv_ptr)
{
    char *argstr_flat;
    wchar_t **argv_w;
    int i, buffsize = 0, offset = 0;

    if (win32_argv_utf8) {
        *argc_ptr = win32_argc;
        *argv_ptr = win32_argv_utf8;
        return;
    }

    win32_argc = 0;
    argv_w = CommandLineToArgvW(GetCommandLineW(), &win32_argc);
    if (win32_argc <= 0 || !argv_w)
        return;

    /* determine the UTF-8 buffer size (including NULL-termination symbols) */
    for (i = 0; i < win32_argc; i++)
        buffsize += WideCharToMultiByte(CP_UTF8, 0, argv_w[i], -1,
                                        NULL, 0, NULL, NULL);

    win32_argv_utf8 = av_mallocz(sizeof(char *) * (win32_argc + 1) + buffsize);
    argstr_flat     = (char *)win32_argv_utf8 + sizeof(char *) * (win32_argc + 1);
    if (!win32_argv_utf8) {
        LocalFree(argv_w);
        return;
    }

    for (i = 0; i < win32_argc; i++) {
        win32_argv_utf8[i] = &argstr_flat[offset];
        offset += WideCharToMultiByte(CP_UTF8, 0, argv_w[i], -1,
                                      &argstr_flat[offset],
                                      buffsize - offset, NULL, NULL);
    }
    win32_argv_utf8[i] = NULL;
    LocalFree(argv_w);

    *argc_ptr = win32_argc;
    *argv_ptr = win32_argv_utf8;
}
#else
inline void prepare_app_arguments(int *argc_ptr, char ***argv_ptr)
{
    /* nothing to do */
}
#endif /* HAVE_COMMANDLINETOARGVW */

int write_option(void *optctx, const OptionDefinition *po, const char *opt, const char *arg)
{
    /* new-style options contain an offset into optctx, old-style address of
     * a global var*/
    void *dst = po->flags & (OPT_OFFSET | OPT_SPEC) ? (uint8_t *)optctx + po->u.off : po->u.dst_ptr;
    int *dstcount;

    if (po->flags & OPT_SPEC)
    {
        SpecifierOpt **so = dst;
        char *p = strchr(opt, ':');
        char *str;

        dstcount = (int *)(so + 1);
        *so = grow_array(*so, sizeof(**so), dstcount, *dstcount + 1);
        str = av_strdup(p ? p + 1 : "");

        if (!str)
        {
            return AVERROR(ENOMEM);
        }

        (*so)[*dstcount - 1].specifier = str;
        dst = &(*so)[*dstcount - 1].u;
    }

    if (po->flags & OPT_STRING)
    {
        char *str;
        str = av_strdup(arg);
        av_freep(dst);

        if (!str)
        {
            return AVERROR(ENOMEM);
        }

        *(char **)dst = str;
    }
    else if (po->flags & OPT_BOOL || po->flags & OPT_INT)
    {
        *(int *)dst = parse_number_or_die(opt, arg, OPT_INT64, INT_MIN, INT_MAX);
    }
    else if (po->flags & OPT_INT64)
    {
        *(int64_t *)dst = parse_number_or_die(opt, arg, OPT_INT64, INT64_MIN, INT64_MAX);
    }
    else if (po->flags & OPT_TIME)
    {
        *(int64_t *)dst = parse_time_or_die(opt, arg, 1);
    }
    else if (po->flags & OPT_FLOAT)
    {
        *(float *)dst = parse_number_or_die(opt, arg, OPT_FLOAT, -INFINITY, INFINITY);
    }
    else if (po->flags & OPT_DOUBLE)
    {
        *(double *)dst = parse_number_or_die(opt, arg, OPT_DOUBLE, -INFINITY, INFINITY);
    }
    else if (po->u.func_arg)
    {
        int ret = po->u.func_arg(optctx, opt, arg);
        if (ret < 0)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to set value '%s' for option '%s': %s\n", arg, opt, av_err2str(ret));
            return ret;
        }
    }

    if (po->flags & OPT_EXIT)
    {
        exit_program(0);
    }

    return 0;
}

int parse_option(void *optctx, const char *opt, const char *arg, const OptionDefinition *options)
{
    const OptionDefinition *po;
    int ret;

    po = find_option(options, opt);

    if (!po->name && opt[0] == 'n' && opt[1] == 'o')
    {
        /* handle 'no' bool option */
        po = find_option(options, opt + 2);

        if ((po->name && (po->flags & OPT_BOOL)))
        {
            arg = "0";
        }
    }
    else if (po->flags & OPT_BOOL)
    {
        arg = "1";
    }

    if (!po->name)
    {
        po = find_option(options, "default");
    }

    if (!po->name)
    {
        av_log(NULL, AV_LOG_ERROR, "Unrecognized option '%s'\n", opt);
        return AVERROR(EINVAL);
    }

    if (po->flags & HAS_ARG && !arg)
    {
        av_log(NULL, AV_LOG_ERROR, "Missing argument for option '%s'\n", opt);
        return AVERROR(EINVAL);
    }

    ret = write_option(optctx, po, opt, arg);

    if (ret < 0)
    {
        return ret;
    }

    return !!(po->flags & HAS_ARG);
}

void parse_options(void *optctx, int argc, char **argv, const OptionDefinition *options, void (*parse_arg_function)(void *, const char*))
{
    const char *opt;

    int optindex, handleoptions = 1, ret;

    // perform system-dependent conversions for arguments list
    prepare_app_arguments(&argc, &argv);

    /* parse options */
    optindex = 1;

    // loop through command line arguments
    while (optindex < argc)
    {
        // get the next command line arguments
        opt = argv[optindex++];

        if (handleoptions && opt[0] == '-' && opt[1] != '\0')
        {
            if (opt[1] == '-' && opt[2] == '\0')
            {
                handleoptions = 0;
                continue;
            }

            opt++;

            if ((ret = parse_option(optctx, opt, argv[optindex], options)) < 0)
            {
                exit_program(1);
            }

            optindex += ret;
        }
        else
        {
            if (parse_arg_function)
            {
                parse_arg_function(optctx, opt);
            }
        }
    }
}

void opt_input_file(void *optctx, const char *filename)
{
    // check if a file name has already been provided in the command line arguments
    if (input_filename)
    {
        av_log(NULL, AV_LOG_FATAL, "Argument '%s' provided as input filename, but '%s' was already specified.\n", filename, input_filename);

        // terminate program execution with 1
        exit(1);
    }

    // if filename is not equal to '-'
    if (!strcmp(filename, "-"))
    {
        filename = "pipe:";
    }

    input_filename = filename;
}

/**
 * Entry point.
 *
 * @param   argc    command line arguments counter.
 * @param   argv    command line arguments.
 *
 * @return          execution exit code.
 */
int main(int argc, char * argv[])
{
    // flags which may be passed to SDL_Init()
    int sdl_flags;

    //
    VideoState * video_state;

    // initialize dynamic library loading
    init_dynload();

    // skip repeated messages when using av_log() instead of (f)printf
    av_log_set_flags(AV_LOG_SKIP_REPEATED);

    // parse and apply the -loglevel command line option
    parse_loglevel(argc, argv, program_options);

    // register all libavdevice codecs, demuxers and protocols
    #if CONFIG_AVDEVICE
        av_log(NULL, AV_LOG_INFO, "Initializing libavdevice and registering all the input and output devices.\n");
        avdevice_register_all();    // [1]
    #endif

    // do a global initialization of network libraries
    avformat_network_init();

    //
    init_opts();

    // Set SIGINT - "interrupt", interactive attention request signal handler
    signal(SIGINT , sigterm_handler);

    // Set SIGTERM - "terminate", termination request signal handler
    signal(SIGTERM, sigterm_handler);

    // show program banner: contains program copyright, version and libs configuration
    show_banner(argc, argv, program_options);

    //
    parse_options(NULL, argc, argv, program_options, opt_input_file);

    // if no input file was provided in the command line arguments
    if (!input_filename)
    {
        // print program usage information
        show_usage();

        // print log messages to stderr warning the user he inputed incorrect command line arguments
        av_log(NULL, AV_LOG_FATAL, "An input file must be specified\n");
        av_log(NULL, AV_LOG_FATAL, "Use -h to get full help or, even better, run 'man %s'\n", program_name);

        // terminate program execution with 1
        exit(1);
    }

    // if output to display is disabled
    if (display_disable)
    {
        video_disable = 1;
    }

    // SDL video, audio and timer subsystems are to be initialized
    sdl_flags = SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER;

    // if audio output is disabled
    if (audio_disable)
    {
        // remove SDL audio subsystem
        sdl_flags &= ~SDL_INIT_AUDIO;
    }
    else
    {
        /*
         * Try to work around an occasional ALSA buffer underflow issue when the
         * period size is NPOT due to ALSA resampling by forcing the buffer size.
         */
        if (!SDL_getenv("SDL_AUDIO_ALSA_SET_BUFFER_SIZE"))
        {
            SDL_setenv("SDL_AUDIO_ALSA_SET_BUFFER_SIZE","1", 1);
        }
    }

    // if output to display is disabled
    if (display_disable)
    {
        // remove SDL video subsystem
        sdl_flags &= ~SDL_INIT_VIDEO;
    }

    // initialize SDL subsystems
    if (SDL_Init(sdl_flags))
    {
        // on failure call SDL_GetError() for more information
        av_log(NULL, AV_LOG_FATAL, "Could not initialize SDL - %s\n", SDL_GetError());
        av_log(NULL, AV_LOG_FATAL, "(Did you set the DISPLAY variable?)\n");

        // terminate program execution with 1
        exit(1);
    }

    // SDL_SYSWMEVENT will automatically be dropped from the event queue
    SDL_EventState(SDL_SYSWMEVENT, SDL_IGNORE);

    // SDL_USEREVENT will automatically be dropped from the event queue
    SDL_EventState(SDL_USEREVENT, SDL_IGNORE);

    // initialize a packet with default values
    av_init_packet(&flush_pkt);

    // data and size members have to be initialized separately
    flush_pkt.data = (uint8_t *)&flush_pkt;

    // if output to display is not disabled
    if (!display_disable)
    {
        int flags = SDL_WINDOW_HIDDEN;

        if (borderless)
        {
            flags |= SDL_WINDOW_BORDERLESS;
        }
        else
        {
            flags |= SDL_WINDOW_RESIZABLE;
        }

        window = SDL_CreateWindow(program_name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, default_width, default_height, flags);
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

        if (window)
        {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (!renderer)
            {
                av_log(NULL, AV_LOG_WARNING, "Failed to initialize a hardware accelerated renderer: %s\n", SDL_GetError());
                renderer = SDL_CreateRenderer(window, -1, 0);
            }
            if (renderer)
            {
                if (!SDL_GetRendererInfo(renderer, &renderer_info))
                {
                    av_log(NULL, AV_LOG_VERBOSE, "Initialized %s renderer.\n", renderer_info.name);
                }
            }
        }

        if (!window || !renderer || !renderer_info.num_texture_formats)
        {
            av_log(NULL, AV_LOG_FATAL, "Failed to create window or renderer: %s", SDL_GetError());
            do_exit(NULL);
        }
    }

    video_state = stream_open(input_filename, file_iformat);

    if (!video_state)
    {
        av_log(NULL, AV_LOG_FATAL, "Failed to initialize VideoState!\n");
        do_exit(NULL);
    }

    event_loop(video_state);

    /* never returns */

    return 0;
}

// [1]
/**
 *  Libavdevice is a complementary library to libavformat. It provides various "special"
 *  platform-specific muxers and demuxers, e.g. for grabbing devices, audio capture
 *  and playback etc. As a consequence, the (de)muxers in libavdevice are of the
 *  AVFMT_NOFILE type (they use their own I/O functions).
 *
 *  The filename passed to avformat_open_input() often does not refer to an actually
 *  existing file, but has some special device-specific meaning - e.g. for xcbgrab
 *  it is the display name.
 *
 *  To use libavdevice, simply call avdevice_register_all() to register all compiled
 *  muxers and demuxers. They all use standard libavformat API.
 */

void show_usage(void)
{
    av_log(NULL, AV_LOG_INFO, "Simple media player\n");
    av_log(NULL, AV_LOG_INFO, "usage: %s [options] input_file\n", program_name);
    av_log(NULL, AV_LOG_INFO, "\n");
}

void set_clock_at(Clock *c, double pts, int serial, double time) {
    c->pts = pts;
    c->last_updated = time;
    c->pts_drift = c->pts - time;
    c->serial = serial;
}

void set_clock(Clock *c, double pts, int serial) {
    double time = av_gettime_relative() / 1000000.0;
    set_clock_at(c, pts, serial, time);
}

void set_clock_speed(Clock *c, double speed) {
    set_clock(c, get_clock(c), c->serial);
    c->speed = speed;
}

void init_clock(Clock *c, int *queue_serial) {
    c->speed = 1.0;
    c->paused = 0;
    c->queue_serial = queue_serial;
    set_clock(c, NAN, -1);
}
