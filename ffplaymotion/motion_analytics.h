#ifndef FFMPEG_VIDEO_PLAYER_MOTION_ANALYTICS_H
#define FFMPEG_VIDEO_PLAYER_MOTION_ANALYTICS_H

#include <stdint.h>

uint64_t detectMotion(uint8_t *data, int width, int stride, int height);

#endif //FFMPEG_VIDEO_PLAYER_MOTION_ANALYTICS_H
